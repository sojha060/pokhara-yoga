$(window).scroll(function() {    
var scroll = $(window).scrollTop();

if (scroll >= 200) {
    $("header").addClass("fixed-top");
} else {
    $("header").removeClass("fixed-top");
}
}); 



//MENU DROPDOWN
     $(document).ready(function () {
$('.navbar .dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    }, function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
    });
});


//MENU DROPDOWN
     $(document).ready(function () {
$('.mobile-menus .dropdown').hover(function () {
        $(this).find('.dropdown ul').first().stop(true, true).slideDown(150);
    }, function () {
        $(this).find('.dropdown ul').first().stop(true, true).slideUp(105)
    });
});


//MOBILE MENU

function openNav() {
  document.getElementById("mySidenav").style.width = "300px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}


//BACK TO TOP
      $(document).ready(function(){

$(function(){
 
    $(document).on( 'scroll', function(){
 
      if ($(window).scrollTop() > 100) {
      $('.scroll-top-wrapper').addClass('show');
    } else {
      $('.scroll-top-wrapper').removeClass('show');
    }
  });
 
  $('.scroll-top-wrapper').on('click', scrollToTop);
});
 
function scrollToTop() {
  verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
  element = $('body');
  offset = element.offset();
  offsetTop = offset.top;
  $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}

});



//GALLERY PAGE

$(document).ready(function() {

  $('.link-gallery').click(function(){
    var descripcion = $(this).attr('title');
    $('#caption').html(descripcion);
      var img = $(this).find('img');
      var src = img.attr('src')
      $('#img01').attr('src', src);
    $('#myModal').css('display','block');
    $('.modal-backdrop').remove();
  });

  $('.close').click(function(){
    $('#myModal').css('display','none');
  });
  
});

//FAQS

 $(".set > a").on("click", function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this)
          .siblings(".set .content")
          .slideUp(200);
        $(".set > a i")
          .removeClass("fa-minus")
          .addClass("fa-plus");
      } else {
        $(".set > a i")
          .removeClass("fa-minus")
          .addClass("fa-plus");
        $(this)
          .find("i")
          .removeClass("fa-plus")
          .addClass("fa-minus");
        $(".set > a").removeClass("active");
        $(this).addClass("active");
        $(".set .content").slideUp(200);
        $(this)
          .siblings(".set .content")
          .slideDown(200);
      }
    });