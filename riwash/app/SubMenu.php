<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
   public function pageInfo(){
    	return $this->hasOne('App\AllPage','id','child_id');
    }
}
