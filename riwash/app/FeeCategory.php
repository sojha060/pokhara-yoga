<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeCategory extends Model
{
    public function feeList(){
    	return $this->hasMany('\App\FeeList', 'category_id', 'id');
    }
}
