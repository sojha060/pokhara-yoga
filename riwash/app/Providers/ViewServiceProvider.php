<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Menu;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       view()->composer('front.includes.header', function($view){
            
            $setting = \App\Setting::first();
            $ourTeam = \App\TeamCategory::all();
            $yogaClass = \App\YogaClass::orderBy('order', 'asc')->get();
            $allPages = \App\AllPage::orderBy('order', 'asc')->get();
            
            $public_menu = Menu::getByName('header'); //return array
            $view->with(compact('setting', 'ourTeam', 'public_menu', 'yogaClass', 'allPages'));
        });




       view()->composer('front.includes.footer', function($view){
           $setting = \App\Setting::first();
            $yogaClass = \App\YogaClass::orderBy('order', 'asc')->get();
            $allPages = \App\AllPage::orderBy('order', 'asc')->get();
            
            $aboutUs = \App\AboutUs::latest()->take(2)->get();
             $courses = \App\Course::all();
            $menu = \App\OurMenu::where('name' , 'footer-menu')->with('pageMenu')->first();

            $view->with(compact('aboutUs', 'courses', 'setting', 'menu', 'yogaClass'));
           
            
           

        });
      
       //this is for get post and events in slider
       view()->composer('front.includes.post-sidebar', function($view){
            
            $notices = \App\Course::orderBy('created_at','desc')->get();
            $events = \App\Notice::orderBy('created_at','desc')->get();
            
            $view->with(compact('notices', 'events'));
        });

       //this is for get post and events in slider
       view()->composer('front.includes.event-sidebar', function($view){
            
            
            $events = \App\Notice::orderBy('created_at','desc')->get();
            
            $view->with(compact( 'events'));
        });

        //this is for banner
       view()->composer('front.includes.banner', function($view){
            
            
            $banner = \App\Banner::first();
            
            $view->with(compact( 'banner'));
        });






    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
