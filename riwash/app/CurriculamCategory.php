<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurriculamCategory extends Model
{
    public function curriculamList(){
    	return $this->hasMany('\App\CurriculamList', 'category_id', 'id');
    }
}
