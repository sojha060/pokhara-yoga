<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeList extends Model
{
    public function feeCategory(){
    	return $this->belongsTo('\App\FeeCategory', 'category_id', 'id');
    }
}
