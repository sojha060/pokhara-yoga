<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurriculamList extends Model
{
    public function category(){
    	return $this->belongsTo('\App\CurriculamCategory', 'category_id', 'id');
    }
}
