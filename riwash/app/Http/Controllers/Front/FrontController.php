<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use App\AboutUs;
use App\OurTeam;
use App\Testimonial;
use App\PhotoList;
use App\BookRoom;
use App\ContactUs;
use App\Course;
use App\Notice;
use App\TeamCategory;
use App\NaraMember;
use App\AllPage;
use App\Blog;
use App\WhyChooseUs;
use App\Facilitie;
use App\Feature;
use App\Setting;
use App\FeeCategory;
use App\YogaClass;
use App\Faq;
use App\Curriculam;
use App\CurriculamCategory;
use App\SeoMeta;
use App\Mail\SendMail;
use Mail;
class FrontController extends Controller
{
    public function index(){
    	$logos = \App\Logo::all();
    	$sliders = \App\Slider::all();
    	$aboutUs = \App\AboutUs::first();
    	$testimonials = \App\Testimonial::all();
    	$photoList = \App\PhotoList::latest()->take(8)->get();
    	$trainings = \App\Course::latest()->get();
    	$events = \App\Notice::latest()->take(4)->get();
    	$teachers = \App\OurTeam::orderBy('order', 'asc')->get();
    	$blogs = Blog::latest()->take(3)->get();
    	$whyChooseUs = WhyChooseUs::first();
    	$facilities = Facilitie::orderBy('order', 'asc')->get();
    	$features = Feature::orderBy('order', 'asc')->get();
    	$feeCategory = FeeCategory::orderBy('created_at', 'asc')->with('feeList')->get();
    	$seoMeta = SeoMeta::where('name', 'index')->first();

            return view('front.index', compact('sliders', 'aboutUs', 'testimonials', 'photoList','events','logos', 'trainings', 'teachers', 'blogs', 'whyChooseUs', 'facilities', 'features', 'feeCategory', 'seoMeta'));
    }
    public function aboutUs(){
    	$aboutUs = \App\AboutUs::all();
    	$ourTeam = \App\OurTeam::all();
    	return view('front.about-us', compact('aboutUs', 'ourTeam'));
    }

   // public function ourTeam(){
   // 	$categories = \App\TeamCategory::all();
   //  	return view('front.our-team', compact('categories'));

   // }

public function ourTeam(){
   	$lists = \App\OurTeam::orderBy('order' ,'asc')->get();

       	return view('front.our-team-list', compact('lists'));

   }

   public function naraMember(){
   	$naraMember = \App\NaraMember::all();
   	return view('front.nara-member', compact('naraMember'));
   }



    public function photoList(){
    	$seoMeta = SeoMeta::where('name', 'gallery')->first();
    	$photoList = \App\PhotoList::orderBy('created_at','desc')->get();
    	return view('front.photo-list', compact('photoList', 'seoMeta'));
    }
    public function room(){
    	return view('front.room');
    }
    public function contactUs(){
    $seoMeta = SeoMeta::where('name', 'contact-us')->first();

    	$setting = Setting::first();
    	$aboutUs = AboutUs::first();
    	return view('front.contact-us', compact('setting', 'aboutUs', 'seoMeta'));
    }

public function contactUsPost(Request $request){
	$this->validate($request, [
		'name' => 'required',
		'email' => 'required',
		'number' => 'required',
		'subject' => 'required',
		'message' => 'required',

	]);

	$information = new \App\ContactUs;
	$information->name = $request->name;
	$information->email = $request->email;
	$information->number = $request->number;
	$information->subject = $request->subject;
	$information->message = $request->message;
	$information->save();

	$data = array(
          'name' => $request->name,
        'email' => $request->email,
        'number' => $request->number,
        'subject' => $request->subject,
        'message' => $request->message,
        
        

        );
$email ='yogaschoolpokhara@gmail.com';
$myEmail = 'technicalriwash@gmail.com';

        
        
        mail::to($email)->send(new SendMail($data));
	return redirect('/contact-us')->with('msg', 'Thanks For your message');
}

// this is check book
public function checkBook(Request $request){
	$request->session()->put('check_in', $request->input('check_in'));
	$checkIn = $request->session()->get('check_in');
	$request->session()->put('check_out', $request->input('check_out'));
	$checkOut = $request->session()->get('check_out');
	$request->session()->put('children', $request->input('children'));
	$children = $request->session()->get('children');
	$request->session()->put('room', $request->input('room'));
	$room = $request->session()->get('room');

	
	
	return view('front.get-book', compact('checkIn', 'checkOut', 'children', 'room'));

}
public function postBook(Request $request){
	$this->validate($request, [

    'name' => 'required',
    'email' => 'required',
    'address' => 'required',
    'number' => 'required',
    'room' => 'required',
    'room_type' => 'required',
    'check_in' => 'required',
    'check_out' => 'required',
	]);

	$information = new \App\BookRoom;
	$information->name = $request->name;
	$information->email = $request->email;
	$information->address = $request->address;
	$information->number = $request->number;
	$information->room = $request->room;
	$information->room_type = $request->room_type;
	$information->check_in = $request->check_in;
	$information->check_out = $request->check_out;
	$information->save();
	

	return redirect('/')->with('msg', 'Room Is Booked. For More Information Please Contact Us');

}

public function newsEvent(){
	$notices = \App\Notice::orderBy('created_at', 'desc')->paginate(10);
	
	return view('front.news-event', compact('notices'));
}

public function Course(){
	$courses = \App\Course::orderBy('created_at', 'desc')->paginate(10);
	
	return view('front.course', compact('courses'));
}

public function singlePost($id){
	$list = \App\Course::find($id);

	return view('front.single-post', compact('list'));

}

public function riverSystem(){
	return view('front.river-system');
}

public function raftingKayaking(){
	return view('front.rafting-kayaking');


}
//single page
public function singlePage($slug){
	$information = AllPage::where('slug', '=', $slug)->first();
	return view('front.single-page', compact('information'));
}

public function singleCourse($slug){
	$singlePage = Course::where('slug', '=', $slug)->first();
	return view('front.single-page', compact('singlePage'));
}

public function singleEvent($slug){
$singlePage = Notice::where('slug', '=', $slug)->first();
	return view('front.single-page', compact('singlePage'));
}

public function singleBlog($slug){
$information = Blog::where('slug', '=', $slug)->first();
	return view('front.single-page', compact('information'));
}

public function blogs(){
	$seoMeta = SeoMeta::where('name', 'blog')->first();
	$blogs = Blog::latest()->paginate(15);
	return view('front.blog', compact('blogs', 'seoMeta'));
}


public function yogaClass($slug){
	$information = YogaClass::where('slug', $slug)->first();
	return view('front.class', compact('information'));

}

public function faq(){
    $seoMeta = SeoMeta::where('name', 'faqs')->first();
	$faqs = Faq::orderBy('created_at', 'asc')->get();
	return view('front.faq', compact('faqs', 'seoMeta'));
}

public function curriculam(){
	$information = Curriculam::first();
	$categories = CurriculamCategory::with('curriculamList')->get();
	return view('front.curriculam', compact('information', 'categories'));
}

public function teacher($id){
	$information = OurTeam::find($id);
	return view('front.teacher', compact('information'));
}

public function training($slug){
	$information = Course::where('slug', $slug)->first();
	
	return view('front.training', compact('information'));
}



}

