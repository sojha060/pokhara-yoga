@include('front.includes.header')



 
			<!-- start banner Area -->
		<div class="page-header mb-5" style="background: url({{asset('uploads/191229091622561ec810e246e055d9de45c2378d5449.jpg')}});background-repeat: no-repeat; background-position: center; background-size: cover;    height: 400px;">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center  " style="padding-top: 200px; color: #fff">Rafting Kayaking</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header --><!-- .page-header -->
			<!-- End banner Area -->	

			<!-- Start feature Area -->





<div class="container-fluid">
    <div class="container">

        <h2 class="py-3 text-uppercase">Rafting & Kayaking in Nepal</h2>

        <div class="card">

            <div class="row">
                <div class="col-sm-6">
                    <p class="text-justify">Rafting, Kayaking, Cannoning The fact that you want to raft down Nepal's challenging rivers means that you have long ago graduated from 
                    the nursery school of "Row, row, row your boat". The many 'raftable' rivers in Nepal meander between canyons, villages, and virgin forests, wildlife, like needle 
                    through thread, weaving the country's rich tapestry of ethno-culture and bio-diversity. Originating in the bowels of the Himalayas, these rivers flow across the length 
                    and breadth of Nepal and neighboring India, before emptying mostly into the Bay of Bengal. </p>
                </div>
                <div class="col-sm-5 ml-2">
                    <img src="{{asset('uploads/c10b6a4a3837e4b0f238b06d0f757792.jpg')}}" class="img-fluid" style="width: 100%; height: 250px">
                </div>
                <div class="col-sm-12">

                    <p class="text-justify">Rafting is inarguably the best way of exploring Arcadian Nepal. The waters offer something to everybody:
                    from grade 5-5+ rivers with many raging white water rapids for the brave and the adventurous, to grade 2-3 rivers with a few rapids for the laid-back type. 
                    The beautiful thing is that just about anybody, whether old or very young, can raft. Moreover, it can either be a two-week trip or a 2-3 day trip-you decide.
                    </p>  
                    <p class="text-justify">

It involves teamwork with the river runner barking instructions from his perch on the aft. You can either do participatory rafting, where you and your group are provided with a runner and barebones staff support, or a "luxury safari style" trip where a full team of staff is provided to address your group's every need and want. Mind you, rafting is not the only way to travel downstream. A few companies offer inflatable kayaks, or fiberglass kayaks for hire.</p>
 <p class="text-justify">

An extreme sport popular in Europe, Canoeing is now available in Nepal. Canoeing gives you the freedom to explore some of the most ruggedly beautiful, yet forbidden places in the world. </p>
                </div>

            </div>

            
        </div>
        

            </div>



        </div>







    </div>





</div>





@include('front.includes.footer')

