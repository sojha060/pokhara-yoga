

@section("title", "$seoMeta->title")
@section("keyword", "$seoMeta->meta_keyword")
@section("desc", "$seoMeta->meta_des")


 @section("singlepage")
  @endsection
@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Blogs</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Blogs</li>
         

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="about-body text-justify">
  <div class="teaching-section bg-white">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Our Blogs</h3>
      </div>
      @foreach($blogs as $blog)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        @if(!$blog->image == '')
        <img src="{{asset('uploads/'.$blog->image)}}" alt="Yoga school in nepal">
        @else
        @endif

        <div class="teach-box">
          <h4>{{$blog->title}}</h4>
          <ul>
            <li><i class="fa fa-user"></i> by <a href="#">Admin</a></li>
            <li><i class="fa fa-calendar"></i>{{ $blog->created_at->format('j M, Y') }}</li>
          </ul>
          <p>{{ strip_tags(str_limit($blog->content, 200)) }}</p>
          <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach

    </div>
  </div>
</div>
</div>

<style type="text/css">
  
.faqs-body{
 padding: 0px;
}
</style>


<div class="faqs-body">
  <div class="container">
    <div class="social-icons">
</div>
    <div class="row">
      <div class="col-12 col-sm-12">

      </div>

{{-- @if(is_null($information->mainPageDrop))
   @else
      @foreach($information->mainPageDrop as $drop)
      <div class="col-12 col-sm-12 col-sm-6 col-md-6">
        <div class="accordion-container">
          <div class="set">
            <a href="javascript:void(0)">
              {{ $drop->title }}
              <i class="fa fa-plus"></i>
            </a>
            <div class="content" style="display: none;">
                <p>{!! $drop->content !!}</p>
            </div>
          </div>

         
         
        </div>
      </div>
      @endforeach
   
      @endif
      --}}


      
    </div>
  </div>
</div>





@include('front.includes.footer')