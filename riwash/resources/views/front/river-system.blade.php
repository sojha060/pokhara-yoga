@include('front.includes.header')



 
			<!-- start banner Area -->
		<div class="page-header mb-5" style="background: url({{asset('uploads/1912290757434619b934c7687e599be8bf3c53b2d89f.jpg')}});background-repeat: no-repeat; background-size: cover; background-position: center;   height: 400px;">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center  " style="padding-top: 200px; color: #fff">River System</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header --><!-- .page-header -->
			<!-- End banner Area -->	

			<!-- Start feature Area -->





<div class="container-fluid">
    <div class="container">

        <h2 class="py-3 text-uppercase">River System</h2>

        <div class="card">

            <h5>The rivers of Nepal can be grouped into three categories on the basis of their origin:</h5>
            <ul class="list-inline">
                <li> - Antecedent to Himalaya</li>
                <li> - After the Mahabharat</li>
                <li> - After the Churia range</li>
            </ul>

            <p class="text-justify">Antecedent rivers belong to the period prior to the rise of the Himalaya. These rivers added their tributaries during or after the Himalayan origin alongwith the development of monsoon climate. After the formation of Mahabharat hills, the antecedent rivers changed their courses as Mahabharat stood as a barrier. As a result, most of the rivers changed their courses either to the east or west. Most of these rivers were responsible to deposit the sediments in the Churia basin.</p>
            <p class="text-justify">

The major river systems namely the Koshi, the Karnali and the Gandaki belong to the antecedent group. Rivers originating from the Mahabharat range and cutting through Churia hills come under the second group, these include Kankai, Bagmati, Kamala etc. The third group of rivers originate from the southern face of the Churia hills. For the purpose of commercial rafting, the following rivers are in use. </p>

<ul class="list-inline">
                <li> - Saptakoshi River System (East Nepal)</li>
                <li> - Narayani or Saptagandaki River System (Central Nepal)</li>
                <li> - Karnali River System (West Nepal)</li>
            </ul>


        </div>
        
        <h5 class="py-3 px-5">Eleven rivers in the three river systems are open for tourists for rafting. The rivers are :</h5>

        <div class="card py-5 px-5">
            <h3>Trishuli</h3>
            <div class="row">
                <div class="col-sm-6">
                    <p class="text-justify"> The Trishuli being the most easily accessible river by far a rafting trip on       this river can be made for a varied durations, depending upon the       availability of time and interest. The Trishuli is the most popular river       for rafting followed by the Sun Kosi. The Trishuli has been so popular       because of its accessibility of road along its major part. a Trishuli trip       can be easily managed to end at a place where a wildlife tour begins.</p>

                </div>
                <div class="col-sm-5 ml-5">
                    <img src="{{asset('uploads/c10b6a4a3837e4b0f238b06d0f757792.jpg')}}" class="img-fluid" style="width: 100%; height: 250px">
                </div>

            </div>



        </div>







    </div>





</div>





@include('front.includes.footer')

