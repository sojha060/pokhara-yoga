@section("title", "$seoMeta->title")
@section("keyword", "$seoMeta->meta_keyword")
@section("desc", "$seoMeta->meta_des")


 @section("singlepage")
  @endsection
@include('front.includes.header')

<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "Pokhara yoga School",
  "url": "http://pokharayogaschoolandretreatcenter.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://pokharayogaschoolandretreatcenter.com/search?q={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>

    
{{-- slider --}}
<div class="slider">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <li data-target="#carouselExampleCaptions" data-slide-to="{{$c}}" class="@if($c == 1) active @else @endif"></li>
      @php $c++; @endphp
      
     @endforeach
    </ol>
    <div class="carousel-inner">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <div class="carousel-item @if($c == 1) active @endif">
        <img src="{{asset('uploads/'.$slider->image)}}" class="d-block w-100" alt="Yoga school in nepal">
        <div class="carousel-caption d-none d-md-block">
          <h5>{{$slider->title}} </h5>
          <p>{!! $slider->content !!}</p>
         {{--  <a href="#" class="btn btn-banner1">200hr</a>
          <a href="#" class="btn btn-banner2">300hr</a>
          <a href="#" class="btn btn-banner3">400hr</a>
          <a href="#" class="btn btn-book">Book Now</a> --}}
        </div>
      </div>
      @php $c++; @endphp
      
     @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

{{-- /slider --}}

{{-- about us --}}

<div class="about-section">
  <div class="container">
    <div class="row">
     
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
        <div class="about-box">
          <h4>{{$aboutUs->title}}</h4>
          <p class="text-justify">
          {{ strip_tags(str_limit($aboutUs->content, 2031)) }}
          <a href="{{action('Front\FrontController@aboutUs')}}" class="btn btn-info">Read More.</a>
        </p>
        </div>
      </div>
      
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
        <div class="video-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="video-box">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>      </div>
      </div>
    </div>
  </div>
</div>
{{-- /about us --}}

{{-- fee --}}
<section id="tabs" >
  
  <div class="container">
    <h6 class="section-title h1">Yoga Teacher Training Fees and Schedule</h6>
    <div class="row">
      <div class="col-12 col-sm-12">
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            @php $c = 1; @endphp
            @foreach($feeCategory as $category)
            <a class="nav-item nav-link  @if($c == 1) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home{{$category->id}}" role="tab" aria-controls="nav-home{{$category->id}}" aria-selected="true">{{$category->title}}</a>

@php $c++ @endphp
             @endforeach
          </div>
        </nav>
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
       
     
 @php $c = 1 @endphp
@foreach($feeCategory as $category)


          <div class="tab-pane fade show @if($c == 1) active @endif " id="nav-home{{$category->id}}" role="tabpanel" aria-labelledby="nav-home-tab{{$category->id}}">
     <h5>{{$category->title}} Yoga Teacher Training In Pokhara Nepal Fee & Schedule 2020</h5>
            <div class="table-responsive" id="sailorTableArea">
              <table id="sailorTable" class="table table-striped table-bordered" width="100%" style="color:#fff">
                <thead>
                  <tr>
                    <th scope="col">From</th>
                    <th scope="col">To</th>
                    <th scope="col">Place</th>
                    <th scope="col">Share Room</th>
                    <th scope="col">Private Room</th>
                    <th scope="col">Enquiry</th>
                  </tr>
                </thead>
                <tbody>
                   
                  @foreach($category->feeList->sortBy('order') as $list)
                    <tr>
                      <td>{{$list->from}}</td>
                      <td>{{$list->to}}</td>
                      <td>{{$list->place}}</td>
                      <td>{{$list->share_room}} USD</td>
                      <td>{{$list->private_room}} USD</td>
                      <td><a href="{{-- {{action('Front\FrontController@applyNow',$list->id)}} --}}" class="apply">  Apply <img src="{{ asset('uploads/new1.gif') }}" alt="Yoga school in nepal"></a></td>
                    </tr>
                     @endforeach
                   
                   
                    
                </tbody>
              </table>
            </div>
          </div>
@php $c++ @endphp
         
          @endforeach





         
   
        </div>
      
      </div>
      <div class="col-12 col-sm-12">
      
      </div>
    </div>
  </div>
</section>

{{-- /fee --}}

{{-- yoga teacher --}}
@if(!$teachers ->isEmpty())
<div class="yoga-teacher">
  <div class="container px-5">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Our Yoga Teachers</h4>
        <p>For quite a long while, our particularly talented teachers have been controlling and affecting the understudies to turn into the leader of their own lives and to stir their own inward holiness. The yogic way of thinking, engaging encounters, and monstrous information on our instructors will urge you to make an effective change in your locale.</p>
      </div>
      @foreach($teachers  as $team)
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
        <img src="{{asset('uploads/'.$team->image)}}" alt="{{ $team->name }}"  class="team-img">
        <h6>{{$team->name}}</h6>
        <a href="{{action('Front\FrontController@teacher',$team->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
      </div>
      @endforeach
      
      
      <div class="col-12 col-sm-12">
        <a href="#" class="btn btn-teacher wow shake" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: shake;">View More Teacher <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif

{{-- /yoga teacher --}}
{{-- trainning --}}

@if($trainings->isEmpty())
@else
<div class="teaching-section">
  <div class="container">
    <dive class="row">
      <div class="col-12 col-sm-12">
        <h3>Yoga Teacher Training In Nepal</h3>
      </div>

      @foreach($trainings as $other)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft; ">
        <img src="{{asset('uploads/'.$other->image)}}" alt="Yoga school in nepal" >
        <div class="teach-box">
          <h4>{{$other->title}}</h4>
          <p>{{ str_limit(strip_tags($other->content), 200) }}</p>
          <a href="{{action('Front\FrontController@training',$other->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
  
</div>
@endif

{{-- /trainning --}}

{{-- test --}}
@if(!$testimonials->isEmpty())
<div class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Yoga Teacher Training Student Testimonials</h4>
      </div>
      @foreach($testimonials as $test)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <div class="testi-box">
          <img src="{{asset('uploads/'.$test->image)}}" alt="Yoga school in nepal">
          <h6><i class="fa fa-quote-left"></i> {{$test->name}}</h6>
          <p>{!! $test->content !!}</p>
          <p><strong>Thank You</strong></p>
        </div>
      </div>
      @endforeach

    
      <div class="col-12 col-sm-12">
        <a href="{{-- {{action('Front\FrontController@testimonial')}} --}}" class="btn btn-testi">Read More Testimonials <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif


{{-- /test --}}

    {{-- why pokhara yoga --}}

<div class="teacher-training">
  <div class="container">
    <div class="row">
      
      
      <div class="col-12 col-sm-12 wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
        <div class="tech-box">
          <h4>{{$whyChooseUs->title}}</h4>
          <p>{!!  $whyChooseUs->content !!}</p>
        </div>
      </div>
      
    </div>
  </div>
</div>
    {{-- /why --}}

    {{-- this is for gallery --}}
<section>
  <div class="container">
    <h3 class="text-center pt-3">Our Gallery</h3>
    <div class="row">
      {{-- single col --}}
      @foreach($photoList as $photo)
      <div class="col-sm-3 py-3">
        <a href="{{asset('uploads/'.$list->image)}}" class="img-gal link-gallery" data-lightbox="roadtrip">
        <div class="card photo-list">
          <img src="{{ asset('uploads/'.$photo->image) }}" alt="Pokhara yoga school" class="w-100 img-fluid" >
        </div>
      </a>

      </div>
      @endforeach
      {{-- /single col --}}

    </div>

  </div>
  
</section>

{{-- blog --}}
@if(!$blogs->isEmpty())
<div class="teaching-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Our Blogs</h3>
      </div>
      @foreach($blogs as $blog)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        @if(!$blog->image == '')
        <img src="{{asset('uploads/'.$blog->image)}}" alt="Yoga school in nepal">
        @else
        @endif

        <div class="teach-box">
          <h4>{{$blog->title}}</h4>
          <ul>
            <li><i class="fa fa-user"></i> by <a href="#">Admin</a></li>
            <li><i class="fa fa-calendar"></i>{{ $blog->created_at->format('j M, Y') }}</li>
          </ul>
          <p>{!! str_limit($blog->content, 200) !!}</p>
          <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach

    </div>
  </div>
</div>
@else
@endif



  @include('front.includes.footer')