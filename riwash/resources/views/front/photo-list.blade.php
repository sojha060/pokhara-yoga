
@section("title", "$seoMeta->title")
@section("keyword", "$seoMeta->meta_keyword")
@section("desc", "$seoMeta->meta_des")
@section("photolist")
  @endsection
@include('front.includes.header')


  
			<!-- start banner Area -->
			<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Gallery</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Gallery</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
			<!-- End banner Area -->	
				
			<!-- Start gallery Area -->
			
			<section class="gallery-area section-gap mt-5 mb-5">
				<div class="container">
					<div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
					<div class="row">
						
						@foreach($photoList as $list)
						<div class="col-lg-4">
							<div class="box">    
							<a href="{{asset('uploads/'.$list->image)}}" class="img-gal link-gallery" data-lightbox="roadtrip">
								<div class="single-imgs relative">		
									<div class="overlay overlay-bg"></div>
									<div class="relative">					
										<img class="img-fluid img-gallery modal-img pt-3" src="{{asset('uploads/'.$list->image)}}" alt="Pokhara yoga School" >				
									</div>
								</div>
							</a>
						</div>
					</div>
				
						@endforeach
						
						
					</div>
				</div>	
			</section>
			<!-- End gallery Area -->
													

			










@include('front.includes.footer')