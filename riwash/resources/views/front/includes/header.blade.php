   <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
   <html lang="en">
   <head>
    
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
    <title> @yield('title')</title>
   
    <meta property="og:url"           content="{{url()->current()}}" />
  <meta property="og:type"          content="Pokhara Yoga School" />
  <meta property="og:title"         content=" @yield('title')" />
  <meta property="og:description"   content=" @yield('desc')" />
  <meta property="og:image" content="{{ asset('uploads/'.$setting->logo) }}" />
  <meta name="author" content="pokhara yoga school">
  <meta name="description" content="@yield('desc')">
  <meta name="keywords" content=" @yield('keyword')">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
   <link rel="canonical" href="https://pokharayogaschoolandretreatcenter.com" hreflang="en">
   <meta name="google-site-verification" content="Oz-mSFiSV1mePMCUtC3CtHSRN177F0hZpB-zWTZZ9qo" />
    <!--Bootstrap CSS-->
    <link rel="icon" href="{{ asset('uploads/'.$setting->logo) }}" type="image/gif" sizes="16x16" hreflang="en">

    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}" hreflang="en">

    <!--Font Awesome CSS-->
  {{--   <link rel="stylesheet" href="{{asset('front/css/all.css')}}" hreflang="en"> --}}

    <!--Main CSS-->
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}" hreflang="en">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" hreflang="en">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" hreflang="en">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet" hreflang="en">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet" hreflang="en">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.css" hreflang="en">

    <!--Owl Carousel-->
    <link rel="stylesheet" href="{{asset('front/css/owl.carousel.min.css')}}" hreflang="en">
    <link rel="stylesheet" href="{{asset('front/css/owl.theme.default.min.css')}}" hreflang="en">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158891144-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158891144-1');
</script>


<!-- google translate -->
<script type="text/javascript">


function googleTranslateElementInit() {
            new google.translate.TranslateElement({ pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false }, 'google_translate_element');
        }


</script>




<!--<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->


<script>
    function translateLanguage(lang) {

        var $frame = $('.goog-te-menu-frame:first');
        if (!$frame.size()) {
            alert("Error: Could not find Google translate frame.");
            return false;
        }
        $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();
        return false;
    }
</script>


 <!--<script>
// //     $('document').ready(function () {
// //         $('#google_translate_element').on("click", function () {

// //             // Change font family and color
// //             $("iframe").contents().find(".goog-te-menu2-item div, .goog-te-menu2-item:link div, .goog-te-menu2-item:visited div, .goog-te-menu2-item:active div") //, .goog-te-menu2 *
// //             .css({
// //                 'color': '#544F4B',
// //                 'background-color': '#e3e3ff',
// //                 'font-family': '"Open Sans",Helvetica,Arial,sans-serif'
// //             });

// //             // Change hover effects  #e3e3ff = white
// //             $("iframe").contents().find(".goog-te-menu2-item div").hover(function () {
// //                 $(this).css('background-color', '#17548d').find('span.text').css('color', '#e3e3ff');
// //             }, function () {
// //                 $(this).css('background-color', '#e3e3ff').find('span.text').css('color', '#544F4B');
// //             });

// //             // Change Google's default blue border
// //             $("iframe").contents().find('.goog-te-menu2').css('border', '1px solid #17548d');

// //             $("iframe").contents().find('.goog-te-menu2').css('background-color', '#e3e3ff');

// //             // Change the iframe's box shadow
// //             $(".goog-te-menu-frame").css({
// //                 '-moz-box-shadow': '0 3px 8px 2px #666666',
// //                 '-webkit-box-shadow': '0 3px 8px 2px #666',
// //                 'box-shadow': '0 3px 8px 2px #666'
// //             });
// //         });
// //     });
// // </script> -->


<!-- google translate  css -->

 <style> 

 /*google translate */
         /*OVERRIDE GOOGLE TRANSLATE WIDGET CSS BEGIN */
        div#google_translate_element div.goog-te-gadget-simple {
            border: none;
            background-color: transparent;
            background-color: #17548d;*/ /*#e3e3ff */
        }

        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value:hover {
            text-decoration: none;
        }

        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span {
            color: #aaa;
        }

        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover {
            color: white;
        }

        .goog-te-gadget-icon {
            display: none !important;
            background: url("url for the icon") 0 0 no-repeat !important;
        }

         /*Remove the down arrow */
         /*when dropdown open */
        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span[style="color: rgb(213, 213, 213);"] {
            display: none;
        }
         /*after clicked/touched */
        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span[style="color: rgb(118, 118, 118);"] {
            display: none;
        }
         on page load (not yet touched or clicked) 
        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span[style="color: rgb(155, 155, 155);"] {
            display: none;
        }

         /*Remove span with left border line | (next to the arrow) in Chrome & Firefox */
        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span[style="border-left: 1px solid rgb(187, 187, 187);"] {
            display: none;
        }
         /*Remove span with left border line | (next to the arrow) in Edge & IE11 */
        div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span[style="border-left-color: rgb(187, 187, 187); border-left-width: 1px; border-left-style: solid;"] {
            display: none;
        }
         /*HIDE the google translate toolbar */
        .goog-te-banner-frame.skiptranslate {
            display: none !important;
        }
        body {
            top: 0px !important;
        }
</style> 

</head>
<body>
<!--  <div id="fb-root"></div>-->
<!--<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.j s#xfbml=1&version=v6.0"></script>-->
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>

<header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <div class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 header-left">
          <ul>
            <li><a href="#"><i class="fa fa-phone-volume"></i> {{ $setting->number }}</a></li>
            <li><a href="mailto:{{ $setting->email }}"><i class="fa fa-envelope"></i> {{ $setting->email }}</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-2 col-lg-2 ">
    <nav class="navbar navbar-expand-lg navbar-light">


          
          

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
             
             
              <li class="nav-item dropdown " >
                <a class="nav-link" href="" style="padding:1px; line-height:30px; color:#fff">Language</a>
                <ul >
                  
                  <li><a href="https://pokharayogaschoolandretreatcenter.com/">En</a></li>
                  <li><a href="https://sp.pokharayogaschoolandretreatcenter.com/">Sp</a></li>
                  
                  
                </ul>
              </li>
              

            <li style="margin-left:19px;" id="google_translate_element"></li>

              
            </ul>
          </div>
        </nav> 

        </div>
        <div class="col-12 col-sm-12 col-md-2 col-lg-2 header-right">
          <ul class="wow shake" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: shake;">

            <li><a href="{{ action('Front\FrontController@contactUs') }}" class="applys mt-3" hreflang="en">Contact Now <i class="fas fa-pen-square"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="menu-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 ">
        <nav class="navbar navbar-expand-lg navbar-light">


          <a class="navbar-brand" href="{{action('Front\FrontController@index')}}"><img src="{{asset('uploads/'.$setting->logo)}}" alt=""></a>
          

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{action('Front\FrontController@index')}}">Home</a>
              </li>
            
             
              <li class="nav-item dropdown ">
                <a class="nav-link" href="">Yoga Courses</a>
                <ul>
                  @foreach($yogaClass as $class)
                  <li><a href="{{ action('Front\FrontController@yogaClass',$class->slug) }}">{{$class->title}}</a></li>
                  @endforeach
                  
                </ul>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@curriculam')}}">Curriculam</a>
              </li>
                @foreach($allPages as $page)
               <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@singlePage',$page->slug)}}">{{ $page->title }}</a>
              </li>
              @endforeach


              <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@blogs')}}">Blogs</a>
              </li>
               <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@photoList')}}">Gallery</a>
              </li>
              {{-- <!-- <li class="nav-item ">-->
              <!--  <a class="nav-link" href="{{action('Front\FrontController@blog')}}">Blog </a>-->
              <!--</li>--> --}}
              <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@contactUs')}}">Contact Us</a>
              </li>
            </ul>
          </div>
        </nav>
        <div class="mobile-header">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-12">
              <span class="clickmenus" onclick="openNav()">&#9776; </span>
              <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="mobile-menus">
                  <ul>
                    <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
                    
 <li >
                <a  href="{{action('Front\FrontController@curriculam')}}">Curriculam</a>
              </li>

              <li>
                      <a href="#"  type="text" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"  >Yoga Courses   <i class="wsmenu-arrow fa fa-angle-down "></i> </a>
                      <div class="collapse multi-collapse" id="multiCollapseExample2">
                        <div class="card card-body">
                        <ul>
                          @foreach($yogaClass as $class)
                  <li><a href="{{ action('Front\FrontController@yogaClass',$class->slug) }}">{{$class->title}}</a></li>
                  @endforeach
                    
                        </ul>
                      </div>
                    </li>

                    @foreach($allPages as $page)
               <li class=" ">
                <a class="" href="{{action('Front\FrontController@singlePage',$page->slug)}}">{{ $page->title }}</a>
              </li>
              @endforeach
                <li class=" ">
                <a class="" href="{{action('Front\FrontController@blogs')}}">Blogs</a>
              </li>
               <li class=" ">
                <a class="" href="{{action('Front\FrontController@photoList')}}">Gallery</a>
              </li>
              
              <li class="">
                <a class="" href="{{action('Front\FrontController@contactUs')}}">Contact Us</a>
              </li>
                       
                

                   
                    
                    
                  </ul>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="col-12 col-sm-12 col-md-2 col-lg-2">
        <div class="menu-adds">
          {{-- <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p>
          {{-- <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
      <div class="col-12 col-sm-12">
        <div class="menu-adds-mobile">
         {{--  <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p> --}}
         {{--  <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
    </div>
  </div>
</header>