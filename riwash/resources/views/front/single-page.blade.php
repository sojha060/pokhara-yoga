

@section("title", "$information->title")
@section("keyword", "$information->meta_keyword")
@section("desc", "$information->meta_des")


 @section("singlepage")
  @endsection
@include('front.includes.header')

<style type="text/css" media="screen">
  .page-banner{
    @if(is_null($information->image))
  background: url("https://pokharayogaschoolandretreatcenter.com/uploads/IMG20191103125404.jpg") center no-repeat;
  @else
  background: url({{ asset('uploads/'.$information->image) }}) center no-repeat;
 @endif
  background-size: cover;
  display: table;
  width: 100%;
  height: 400px;
}
</style>

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>{{$information->title}}</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>{{$information->title}}</li>
         

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="about-body text-justify">
  <div class="container">
  
          

    <div class="row">
     
      <div class="col-12 col-sm-12  col-md-8 col-lg-8  single-page">
        @if($information->name)
        <h1> <strong>Name:</strong><span class="pr-1"> </span>{{$information->name}}</h1>
@else 
@endif
        <p>{!!  $information->content !!}</p>

        
      </div>

   
   <div class="col-sm-4">
    <div class="card py-2 px-2">
    <h3 class="py-2" style="color: #026B2F">Service Hightlight</h3>
    <p> <div class="fb-page" data-href="https://www.facebook.com/pokharayogaschool/" data-tabs="timeline" data-width="" data-height="320px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pokharayogaschool/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pokharayogaschool/">Pokhra Yoga School</a></blockquote></div></p>
  </div>
   
   </div> 
   
    </div>
  </div>
</div>

<style type="text/css">
  
.faqs-body{
 padding: 0px;
}
</style>


<div class="faqs-body">
  <div class="container">
    <div class="social-icons">
</div>
    <div class="row">
      <div class="col-12 col-sm-12">

      </div>

{{-- @if(is_null($information->mainPageDrop))
   @else
      @foreach($information->mainPageDrop as $drop)
      <div class="col-12 col-sm-12 col-sm-6 col-md-6">
        <div class="accordion-container">
          <div class="set">
            <a href="javascript:void(0)">
              {{ $drop->title }}
              <i class="fa fa-plus"></i>
            </a>
            <div class="content" style="display: none;">
                <p>{!! $drop->content !!}</p>
            </div>
          </div>

         
         
        </div>
      </div>
      @endforeach
   
      @endif
      --}}


      
    </div>
  </div>
</div>





@include('front.includes.footer')