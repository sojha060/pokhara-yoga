<div class="form-group">
    {{ Form::label('category', 'Team Category') }}
    {{ Form::select('team_category_id', $categories, null, ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'required']) }}
</div>

<div class="form-group">
    {{ Form::label('content', 'Content') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'summernote']) }}
</div>

<div class="form-group">
    {{ Form::label('image', 'Image') }}
    {{ Form::file('image', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('order', 'Order') }}
    {{ Form::text('order', null, ['class' => 'form-control', 'required']) }}
</div>



<div class="form-group">
    <button type="submit" class="btn btn-success">Save</button>
</div>
