<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('r-admin/login');
});
Route::prefix('r-admin')->group(function(){
Auth::routes();
});
Route::get('r-admin', function(){
return redirect('r-admin/login');
});
Route::get('r-admin/register', function(){
echo 'website Design / Developed By Riwash Chamlagain Contact 9825909867';
});

Route::get('/home', 'HomeController@index')->name('home');
 

Route::middleware('auth')->group(function() { 
	Route::prefix('admin/')->group(function(){


Route::resource('aboutus', 'Admin\AboutUsController');
Route::resource('testimonial', 'Admin\TestimonialController');
Route::resource('slider', 'Admin\SliderController');
Route::resource('gallery', 'Admin\GalleryController');
Route::resource('photo-list', 'Admin\PhotoListController');
Route::resource('contact', 'Admin\ContactController');
Route::resource('room-book', 'Admin\RoomBookController');
Route::resource('notice', 'Admin\NoticeController');
Route::resource('course', 'Admin\CourseController');
Route::resource('team-category', 'Admin\TeamCategoryController');
Route::resource('our-team', 'Admin\OurTeamController');
Route::resource('nara-member', 'Admin\NaraMemberController');
Route::resource('blog', 'Admin\BlogController');
Route::resource('why-choose-us', 'Admin\WhyChooseUsController');
Route::resource('curriculam', 'Admin\CurriculamController');
Route::resource('curriculam-category', 'Admin\CurriculamCategoryController');
Route::resource('curriculam-list', 'Admin\CurriculamListController');
Route::resource('faq', 'Admin\FaqController');
Route::resource('seo-meta', 'Admin\SeoMetaController');

Route::resource('fee-category', 'Admin\FeeCategoryController');
Route::resource('fee-list', 'Admin\FeeListController');
Route::resource('feature', 'Admin\FeatureController');
Route::resource('facilitie', 'Admin\FacilitieController');
Route::resource('logo', 'Admin\LogoController');
Route::resource('setting', 'Admin\SettingController');
Route::resource('banner', 'Admin\BannerController');
Route::resource('all-page', 'Admin\AllPageController');
Route::resource('menu', 'Admin\MenuController');
Route::resource('yoga-class', 'Admin\YogaClassController');
Route::get('create-menu/{id}', 'Admin\MenuController@createMenu');
Route::post('store-menu', 'Admin\MenuController@storeMenu');
Route::post('destroy-menu/{id}', 'Admin\MenuController@destroyMenu');
Route::get('edit-menu/{id}', 'Admin\MenuController@editMenu');
Route::patch('edit-menu/{id}', 'Admin\MenuController@updateMenu');
Route::post('/sub-menu-add', 'Admin\MenuController@subMenuAdd');
Route::delete('/sub-menu-remove/{id}', 'Admin\MenuController@subMenuRemove');

Route::get('/out-team/delete/{id}', 'Admin\OurTeamController@delete');



	});

});

// this is front controller
Route::get('/', 'Front\FrontController@index');
Route::get('/about-us', 'Front\FrontController@aboutUs');
Route::get('/room', 'Front\FrontController@room');
Route::get('/contact-us', 'Front\FrontController@contactUs');
Route::post('/contact-us', 'Front\FrontController@contactUsPost');
Route::post('/check-book', 'Front\FrontController@checkBook');
Route::post('/post-book', 'Front\FrontController@postBook');
Route::get('/photo-list', 'Front\FrontController@photoList');
Route::get('/single-post/{id}', 'Front\FrontController@singlePost');
Route::get('/news-event', 'Front\FrontController@newsEvent');
Route::get('/event', 'Front\FrontController@event');
Route::get('/river-system', 'Front\FrontController@riverSystem');
Route::get('/rafting-kayaking', 'Front\FrontController@raftingKayaking');
Route::get('/our-team', 'Front\FrontController@ourTeam');
Route::get('/our-team/{id}', 'Front\FrontController@ourTeamList');
Route::get('/nara-member', 'Front\FrontController@naraMember');
Route::get('/single-page/{slug}', 'Front\FrontController@singlePage');
Route::get('/single-course/{slug}', 'Front\FrontController@singleCourse');
Route::get('/single-event/{slug}', 'Front\FrontController@singleEvent');
Route::get('/single-blog/{slug}', 'Front\FrontController@singleBlog');
Route::get('/blog', 'Front\FrontController@blogs');
Route::get('/class/{slug}', 'Front\FrontController@yogaClass');
Route::get('faq', 'Front\FrontController@faq');
Route::get('curriculam', 'Front\FrontController@curriculam');
Route::get('teacher/{id}', 'Front\FrontController@teacher');
Route::get('training/{slug}', 'Front\FrontController@training');




