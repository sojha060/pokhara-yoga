<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllFeeList extends Model
{
    public function allFeeCategory(){
    	return $this->belongsTo(AllFeeCategory::class);
    }
    public function applyClass(){
    	return $this->hasMany(ApplyClass::class);
    }
}
