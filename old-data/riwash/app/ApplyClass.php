<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyClass extends Model
{
    public function allFeeList(){
    	return $this->belongsTo(AllFeeList::class);
    }
}
