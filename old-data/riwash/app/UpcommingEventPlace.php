<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpcommingEventPlace extends Model
{
    public function upcommingEventList(){
    	return $this->hasMany(UpcommingEventList::class);
    }
}
