<?php

namespace App\Http\Controllers;

use App\Admin\AllFeeCategoryController;
use Illuminate\Http\Request;

class AllFeeCategoryControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\AllFeeCategoryController  $allFeeCategoryController
     * @return \Illuminate\Http\Response
     */
    public function show(AllFeeCategoryController $allFeeCategoryController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\AllFeeCategoryController  $allFeeCategoryController
     * @return \Illuminate\Http\Response
     */
    public function edit(AllFeeCategoryController $allFeeCategoryController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\AllFeeCategoryController  $allFeeCategoryController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllFeeCategoryController $allFeeCategoryController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\AllFeeCategoryController  $allFeeCategoryController
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllFeeCategoryController $allFeeCategoryController)
    {
        //
    }
}
