<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubPage;
use App\LastPage;
use File;

class LastPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\LastPage::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.last-page.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $subPage = \App\SubPage::all()->pluck('title', 'id')->toarray();
        return view('admin.last-page.create', compact('subPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $information = new \App\LastPage;
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',

        ]);

       $information->image = '';

       
                if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $information->image = $filename;
      }
     
        $information->title = $request->title;
          $information->order = $request->order;
        $information->des = $request->des;
        $information->keyword = $request->keyword;
        $information->slug = $request->slug;
        
        $information->sub_page_id = $request->sub_page_id;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/last-page')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $information = \App\LastPage::find($id);
        $subPage = \App\SubPage::all()->pluck('title', 'id')->toarray();
        return view('admin.last-page.edit', compact('information','subPage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $information = \App\LastPage::find($id);
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',

        ]);
        $oldfile = $information->image;
      //file upload
      $information->image = $oldfile;
      if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads/';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $oldfile = public_path().'/uploads/'.$oldfile;
         if(File::exists($oldfile))
         {
            File::delete($oldfile);
         }
         $information->image = $filename;
      }
        $information->title = $request->title;
          $information->order = $request->order;
        $information->des = $request->des;
        $information->keyword = $request->keyword;
        $information->slug = $request->slug;
        $information->sub_page_id = $request->sub_page_id;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/last-page')->with('msg', 'Information Upload');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $information = \App\LastPage::find($id);
      $path = public_path().'uploads/'.$information->image;
      if(File::exists($path))
      {
         File::delete($path);
      }

        $information->delete();
        return redirect('admin/last-page')->with('msg', 'Information Deleted');
    }
}
