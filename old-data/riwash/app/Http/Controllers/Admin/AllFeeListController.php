<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AllFeeList;
use App\AllFeeCategory;

class AllFeeListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\AllFeeList::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.all-fee-list.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = \App\AllFeeCategory::all()->pluck('name', 'id')->toarray();
        return view('admin.all-fee-list.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'from' => 'required',
            'to' => 'required',
            'place' => 'required',
            'share_room' => 'required',
            'private_room' => 'required',
        ]);
        $information = new \App\AllFeeList;
        $information->from = $request->from;
        $information->to = $request->to;
        $information->place = $request->place;
        $information->share_room = $request->share_room;
        $information->private_room = $request->private_room;
        $information->all_fee_category_id = $request->all_fee_category_id;
        $information->save();
        return redirect('admin/all-fee-list')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $categories = \App\AllFeeCategory::all()->pluck('name', 'id')->toarray();
         $information = \App\AllFeeList::find($id);
        return view('admin.all-fee-list.edit', compact('information','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'from' => 'required',
            'to' => 'required',
            'place' => 'required',
            'share_room' => 'required',
            'private_room' => 'required',
        ]);
        $information = \App\AllFeeList::find($id);
        $information->from = $request->from;
        $information->to = $request->to;
        $information->place = $request->place;
        $information->share_room = $request->share_room;
        $information->private_room = $request->private_room;
        $information->all_fee_category_id = $request->all_fee_category_id;
        $information->save();
        return redirect('admin/all-fee-list')->with('msg', 'Information Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\AllFeeList::find($id);
        $information->delete();

        return redirect('admin/all-fee-list')->with('msg', 'Information Deleted');
        
    }
}
