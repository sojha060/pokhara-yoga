<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AllfeeCategory;
class AllFeeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $informations = \App\AllFeeCategory::orderBy('created_at', 'desc')->paginate(10);
         return view('admin.all-fee-category.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.all-fee-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:all_fee_categories',
        
        ]);
        $information = new \App\AllFeeCategory;
        $information->name = $request->name;
        $information->title = $request->title;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/all-fee-category')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\AllFeeCategory::find($id);
        return view('admin.all-fee-category.edit', compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
            'name' => 'required',
        
        ]);
        $information = \App\AllFeeCategory::find($id);
        $information->name = $request->name;
        $information->title = $request->title;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/all-fee-category')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\AllfeeCategory::find($id);
        $information->delete();
        return redirect('admin/all-fee-category')->with('msg', 'Information Deleted');

    }
}
