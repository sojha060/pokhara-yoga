<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubPage;
use App\MainPage;
use File;

class SubpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\SubPage::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.sub-page.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainPage = \App\MainPage::all()->pluck('title', 'id')->toarray();
        return view('admin.sub-page.create', compact('mainPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $information = new \App\SubPage;
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',


        ]);

       $information->image = '';

       
                if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $information->image = $filename;
      }
     
        $information->title = $request->title;
        $information->slug = $request->slug;

          $information->order = $request->order;
        $information->des = $request->des;
        $information->keyword = $request->keyword;
        $information->main_page_id = $request->main_page_id;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/sub-page')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\SubPage::find($id);
        $mainPage = \App\MainPage::all()->pluck('title', 'id')->toarray();
        return view('admin.sub-page.edit', compact('information','mainPage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $information = \App\SubPage::find($id);
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',
           

        ]);
        $oldfile = $information->image;
      //file upload
      $information->image = $oldfile;
      if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads/';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $oldfile = public_path().'uploads/'.$oldfile;
         if(File::exists($oldfile))
         {
            File::delete($oldfile);
         }
         $information->image = $filename;
      }
        $information->title = $request->title;
        $information->slug = $request->slug;

          $information->order = $request->order;
        $information->des = $request->des;
        $information->keyword = $request->keyword;
        $information->main_page_id = $request->main_page_id;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/sub-page')->with('msg', 'Information Upload');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\SubPage::find($id);
      $path = public_path().'uploads/'.$information->image;
      if(File::exists($path))
      {
         File::delete($path);
      }

        $information->delete();
        return redirect('admin/sub-page')->with('msg', 'Information Deleted');
    }
}
