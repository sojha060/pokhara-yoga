<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MainPage;
use File;

class MainPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\MainPage::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.main-page.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.main-page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = new \App\MainPage;
        $this->validate($request, [
           'title' => 'required|unique:main_pages',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',

        ]);
      


       $information->image = '';

       
                if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $information->image = $filename;
      }
     
        $information->title = $request->title;
        $information->slug = $request->slug;
        $information->order = $request->order;
        $information->des = $request->des;
        $information->keyword = $request->keyword;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/main-page')->with('msg', 'Information Added');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\MainPage::find($id); 
        return view('admin.main-page.edit', compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $information = \App\MainPage::find($id);
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'order' => 'required',
           'des' => 'required',
           'keyword' => 'required',
           'slug' => 'required',

        ]);
        $oldfile = $information->image;
      //file upload
      $information->image = $oldfile;
      if($request->hasFile('image'))
      {
         $file = $request->file('image');
         $path = public_path().'uploads/';
         $filename = date('ymdhis').$file->getClientOriginalName();
         $file->move($path, $filename);
         $oldfile = public_path().'uploads/'.$oldfile;
         if(File::exists($oldfile))
         {
            File::delete($oldfile);
         }
         $information->image = $filename;
      }
      $information->order = $request->order;
        $information->des = $request->des;
        $information->slug = $request->slug;

        $information->keyword = $request->keyword;
        $information->title = $request->title;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/main-page')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $information = \App\MainPage::find($id);
      $path = public_path().'uploads/'.$information->image;
      if(File::exists($path))
      {
         File::delete($path);
      }

        $information->delete();
        return redirect('admin/main-page')->with('msg', 'Information Deleted');
    }
}
