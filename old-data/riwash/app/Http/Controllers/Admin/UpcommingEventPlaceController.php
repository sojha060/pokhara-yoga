<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UpcommingEventPlace;

class UpcommingEventPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\UpcommingEventPlace::orderBy('created_at', 'desc')->get();
        return view('admin.upcomming-event-place.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.upcomming-event-place.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:upcomming_event_places',
        ]);
       $information = new \App\UpcommingEventPlace;
       $information->name = $request->name;
       $information->save();
        return redirect('admin/upcomming-event-place')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\UpcommingEventPlace::find($id);
        return view('admin.upcomming-event-place.edit', compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $information = \App\UpcommingEventPlace::find($id);
        $information->name = $request->name;
        $information->save();
        return redirect('admin/upcomming-event-place')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $information = \App\UpcommingEventPlace::find($id);
        $information->delete();
        return redirect('admin/upcomming-event-place')->with('msg', 'Information Delated');
    }
}
