<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HourYogaTeacher;

class HourYogaTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\HourYogaTeacher::all();
        return view('admin.hour-yoga-teacher.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hour-yoga-teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'content' => 'required',
        ]);
        $information = new \App\HourYogaTeacher;
        $information->title = $request->title;
        $information->date = $request->date;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/hour-yoga-teacher')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\HourYogaTeacher::find($id);
        return view('admin.hour-yoga-teacher.edit', compact('information'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'content' => 'required',
        ]);
        $information = \App\HourYogaTeacher::find($id);
        $information->title = $request->title;
        $information->date = $request->date;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/hour-yoga-teacher')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\HourYogaTeacher::find($id);
        $information->delete();
        return redirect('admin/hour-yoga-teacher')->with('msg', 'Information Deleted');

        
    }
}
