<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhyPokharaYogaFor;
class WhyPokharaYogaForController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\WhyPokharaYogaFor::all();
        return view('admin.why-prokhara-yoga-for.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.why-prokhara-yoga-for.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $information = new \App\WhyPokharaYogaFor;
        $information->title = $request->title;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/why-prokhara-yoga-for')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = \App\WhyPokharaYogaFor::find($id);
        return view('admin.why-prokhara-yoga-for.edit', compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $information = \App\WhyPokharaYogaFor::find($id);
        $information->title = $request->title;
        $information->content = $request->content;
        $information->save();
        return redirect('admin/why-prokhara-yoga-for')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\WhyPokharaYogaFor::find($id);
        $information->delete();
        return redirect('admin/why-prokhara-yoga-for')->with('msg', 'Information Deleted');

        
    }
}
