<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UpcommingEventList;
use App\UpcommingEventPlace;

class UpcommingEventListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\UpcommingEventList::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.upcomming-event-list.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $places = \App\UpcommingEventPlace::pluck('name', 'id')->toarray();
        return view('admin.upcomming-event-list.create', compact('places'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'price' => 'required',
        ]);
        $information = new \App\UpcommingEventList;
        $information->title = $request->title;
        $information->date = $request->date;
        $information->price = $request->price;
        $information->upcomming_event_place_id = $request->upcomming_event_place_id;
        $information->save();
        return redirect('admin/upcomming-event-list')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $places = \App\UpcommingEventPlace::pluck('name', 'id')->toarray();
       $information = \App\UpcommingEventList::find($id);
        return view('admin.upcomming-event-list.edit', compact('information','places'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'price' => 'required',
        ]);
        $information = \App\UpcommingEventList::find($id);
        $information->title = $request->title;
        $information->date = $request->date;
        $information->price = $request->price;
        $information->upcomming_event_place_id = $request->upcomming_event_place_id;
        $information->save();
        return redirect('admin/upcomming-event-list')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $information = \App\UpcommingEventList::find($id);
        $information->delete();
        return redirect('admin/upcomming-event-list')->with('msg', 'Information Deleted');

        
    }
}
