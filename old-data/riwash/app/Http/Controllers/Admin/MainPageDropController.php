<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MainPage;
use App\MainPageDrop;

class MainPageDropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informations = \App\MainPageDrop::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.main-page-drop.index', compact('informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainPage = \App\MainPage::get()->pluck('title', 'id')->toarray();
        return view('admin.main-page-drop.create', compact('mainPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = new \App\MainPageDrop;
        $information->title = $request->title;
        $information->content = $request->content;
        $information->main_page_id = $request->main_page_id;
        $information->save();
        return redirect('admin/main-page-drop')->with('msg', 'Information Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $information = \App\MainPageDrop::find($id);
        $mainPage = \App\MainPage::get()->pluck('title', 'id')->toarray();
        return view('admin.main-page-drop.edit', compact('information','mainPage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $information = \App\MainPageDrop::find($id);
        $information->title = $request->title;
        $information->content = $request->content;
        $information->main_page_id = $request->main_page_id;
        $information->save();
        return redirect('admin/main-page-drop')->with('msg', 'Information Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           $information = \App\MainPageDrop::find($id);
          $information->delete();
           return redirect('admin/main-page-drop')->with('msg', 'Information Deleted');

    }
}
