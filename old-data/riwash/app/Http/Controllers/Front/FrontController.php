<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use App\AboutUs;
use App\OurTeam;
use App\Testimonial;
use App\PhotoList;
use App\BookRoom;
use App\ContactUs;
use App\Course;
use App\YogaTeacherTrainingNepal;
use App\SpecialOffer;
use App\FactOfOurYoga;
use App\SpecialYogaTraining;
use App\WhyPokharaYogaFor;
use App\HourYogaTeacher;
use App\UpcommingEventPlace;
use App\MainPage;
use App\SubPage;
use App\AllFeeCategory;
use App\AllFeeList;
use App\FAQ;
use App\ApplyClass;
use App\OtherInformation;
use App\Mail\ContactMail;
class FrontController extends Controller
{
    public function index(){
    	$logos = \App\Logo::all();
    	$sliders = \App\Slider::all();
    	$aboutUs = \App\AboutUs::all();
    	$testimonials = \App\Testimonial::latest()->get();
    	$photoList = \App\PhotoList::latest()->take(12)->get();
    	$blogs = \App\Course::latest()->take(9)->get();
    	$yogaTraningNepal = \App\YogaTeacherTrainingNepal::orderBy('created_at', 'desc')->get();
   $specialOffer = \App\SpecialOffer::orderBy('created_at', 'desc')->get();
   $ourTeam = \App\OurTeam::orderBy('created_at', 'desc')->get();
   $uniqueFact = \App\FactOfOurYoga::orderBy('created_at', 'desc')->get();
   $yogaCourse = \App\SpecialYogaTraining::orderBy('created_at', 'desc')->get();
   $whyYoga = \App\WhyPokharaYogaFor::orderBy('created_at', 'desc')->get();
   $hourYoga = \App\HourYogaTeacher::orderBy('created_at', 'desc')->get();
   $events = \App\UpcommingEventPlace::all();
   $otherInformation = \App\OtherInformation::all();
   $feeList = \App\AllFeeCategory::get();


            return view('front.index', compact('sliders', 'aboutUs', 'testimonials', 'photoList','blogs','logos', 'yogaTraningNepal','specialOffer', 'ourTeam', 'uniqueFact','yogaCourse', 'whyYoga', 'hourYoga', 'events', 'otherInformation','feeList'));
    }




    public function aboutUs(){
    	$aboutUs = \App\AboutUs::all();
    	$ourTeam = \App\OurTeam::all();
    	return view('front.about-us', compact('aboutUs', 'ourTeam'));
    }

    public function singleCourse($id){
echo "welcome";
    }
    public function photoList(){
    	$photoList = \App\PhotoList::orderBy('created_at','desc')->get();
    	return view('front.photo-list', compact('photoList'));
    }
    public function room(){
    	return view('front.room');
    }
    public function contactUs(){
    	return view('front.contact-us');
    }

public function contactUsPost(Request $request){
	$this->validate($request, [
		'name' => 'required',
		'email' => 'required',
		'number' => 'required',
		'subject' => 'required',
		'message' => 'required',

	]);

	$information = new \App\ContactUs;
	$information->name = $request->name;
	$information->email = $request->email;
	$information->number = $request->number;
	$information->subject = $request->subject;
	$information->message = $request->message;
	$information->save();
	
	$data = array(
            'name' => $request->name,
        'email' => $request->email,
        'number' => $request->number,
        'subject' => $request->subject,
        'message' => $request->message,
        

        );
$email = 'yogaschoolpokhara@gmail.com';
$myEmail = 'yogaschoolpokhara@gmail.com';

        
        
        mail::to($email)->send(new ContactMail($data));
        
	return redirect('/contact-us')->with('msg', 'Thanks For your message');
}




//this is for single page of yogaTrainingNepal

public function yogaTrainingNepal($id){


	$information = \App\YogaTeacherTrainingNepal::find($id);
	
	
	return view('front.single-page', compact('information'));
}

//this is for teacher
public function teacher($id){
	$information = \App\OurTeam::find($id);
	return view('front.single-page', compact('information'));

}
//this is for uniquefact
public function uniquefact($id){
	$information = \App\FactOfOurYoga::find($id);

	return view('front.single-page', compact('information'));

}

//this is for specialYoga
public function specialYoga($id){
	$information = \App\SpecialYogaTraining::find($id);
	return view('front.single-page', compact('information'));

}
public function blog(){
    $blogs = \App\Course::latest()->get();
    return view('front.blog', compact('blogs'));
}
public function singleBlog($slug){
$information = Course::where('slug', '=', $slug)->first();

return view('front.single-blog', compact('information'));
}
//this is for single page 
public function singlePage($slug){
  $information = SubPage::where('slug', '=', $slug)->first();


	return view('front.single-page', compact('information'));

}

//this is for single Main page 
public function singleMainPage($slug){
  $information = MainPage::where('slug', '=', $slug)->first();
	return view('front.single-page', compact('information'));

}
//this is for single Main page 
public function singleOther($id){
  $information = \App\OtherInformation::find($id);
	return view('front.single-page', compact('information'));

}
public function feeList(){
	$feeList = \App\AllFeeCategory::get();
	return view('front.fee-list', compact('feeList'));
}
public function faqS(){
	$faqS = \App\FAQ::orderBy('created_at', 'desc')->get();
	return view('front.faqs', compact('faqS'));
}
//this is for testimonial
public function testimonial(){
	$testimonials = \App\Testimonial::orderBy('created_at', 'desc')->get();
	return view('front.testimonial', compact('testimonials'));
}
//apply now
public function applyNow($id){
	$information = \App\AllFeeList::find($id);
	return view('front.apply-now', compact('information'));
}

}
