<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastPage extends Model
{
    public function subPage(){
    	return $this->belongsTo(SubPage::class);
    }
}
