<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPage extends Model
{
    public function mainPage(){
    	return $this->belongsTo(MainPage::class);
    }
    public function lastPage(){
    	return $this->hasMany(lastPage::class);
    }
}
