<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       view()->composer('front.includes.header', function($view){
            
            $logos = \App\Logo::all();
            $mainPage = \App\MainPage::orderBy('order', 'asc')->get();
            $metaTags = \App\IndexSEO::get();
            
            $view->with(compact('logos', 'mainPage', 'metaTags'));
        });

       view()->composer('front.includes.blog-sidebar', function($view){
            
            $blogs = \App\Course::orderBy('created_at', 'desc')->paginate(20);
            
            $view->with(compact('blogs'));
        });
        view()->composer('front.includes.footer', function($view){
            
            $logos = \App\Logo::all();
            $mainPage = \App\MainPage::all();
            
            $view->with(compact('logos', 'mainPage'));
        });

        //yogaaa classes
        view()->composer('front.includes.footer', function($view){
            
            $yogaTraningNepal = \App\YogaTeacherTrainingNepal::orderBy('created_at', 'desc')->get();
            $mainPage = \App\MainPage::all();
            $course = \App\MainPage::where('id' , '3')->first();
           
            
            $view->with(compact('yogaTraningNepal', 'mainPage', 'course'));
        });

        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
