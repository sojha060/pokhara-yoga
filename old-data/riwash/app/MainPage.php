<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
    public function subPage(){
    	return $this->hasMany(SubPage::class);
    }
     public function mainPageDrop(){
    	return $this->hasMany(MainPageDrop::class);
    }

}
