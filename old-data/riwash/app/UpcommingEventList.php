<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpcommingEventList extends Model
{
   public function upcommingEventPlace(){
   	return $this->belongsTo(UpcommingEventPlace::class);
   }
}
