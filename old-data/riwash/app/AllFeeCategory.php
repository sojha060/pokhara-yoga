<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllFeeCategory extends Model
{
    public function allFeeList(){
    	return $this->hasMany(AllFeeList::class);
    }
}
