<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('/login');
});
Auth::routes();
Route::get('/register', function(){
echo 'Website Developed By Riwash Chamlagain Contact 9825909867';
});

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->group(function() { 
	// this is prefix
	Route::prefix('admin/')->group(function(){


	
Route::resource('aboutus', 'Admin\AboutUsController');
Route::resource('testimonial', 'Admin\TestimonialController');
Route::resource('slider', 'Admin\SliderController');
Route::resource('gallery', 'Admin\GalleryController');
Route::resource('photo-list', 'Admin\PhotoListController');
Route::resource('contact', 'Admin\ContactController');
Route::resource('room-book', 'Admin\RoomBookController');
Route::resource('notice', 'Admin\NoticeController');
Route::resource('course', 'Admin\CourseController');
Route::resource('our-team', 'Admin\OurTeamController');
Route::resource('logo', 'Admin\LogoController');
Route::resource('why-prokhara-yoga-for', 'Admin\WhyPokharaYogaForController');
Route::resource('hour-yoga-teacher', 'Admin\HourYogaTeacherController');
Route::resource('yoga-teacher-training-nepal', 'Admin\YogaTeacherTrainingNepalController');
Route::resource('fact-of-our-yoga', 'Admin\FactOfOurYogaController');
Route::resource('upcomming-event-place', 'Admin\UpcommingEventPlaceController');
Route::resource('upcomming-event-list', 'Admin\UpcommingEventListController');
Route::resource('special-yoga-training', 'Admin\SpecialYogaTrainingController');
Route::resource('special-offer', 'Admin\SpecialOfferController');
Route::resource('other-information', 'Admin\OtherInformationController');
Route::resource('main-page', 'Admin\MainPageController');
Route::resource('main-page-drop', 'Admin\MainPageDropController');
Route::resource('sub-page', 'Admin\SubPageController');
Route::resource('last-page', 'Admin\LastPageController');
Route::resource('all-fee-category', 'Admin\AllFeeCategoryController');
Route::resource('all-fee-list', 'Admin\AllFeeListController');
Route::resource('faq', 'Admin\FAQController');
Route::resource('seo-index', 'Admin\SeoIndexController');
Route::resource('apply-list', 'Admin\ApplyListController');


});
// /prefix
});

// this is front controller
Route::get('/', 'Front\FrontController@index');
Route::get('/about-us', 'Front\FrontController@aboutUs');
Route::get('/room', 'Front\FrontController@room');
Route::get('/contact-us', 'Front\FrontController@contactUs');
Route::post('/contact-us', 'Front\FrontController@contactUsPost');
Route::post('/check-book', 'Front\FrontController@checkBook');
Route::post('/post-book', 'Front\FrontController@postBook');
Route::get('/photo-list', 'Front\FrontController@photoList');
Route::get('/course/{id}', 'Front\FrontController@singleCourse');
Route::get('/all-blog', 'Front\FrontController@blog');
Route::get('/single-page-yoga-training/{id}', 'Front\FrontController@yogaTrainingNepal');
Route::get('/teacher/{id}', 'Front\FrontController@teacher');
Route::get('/unique-fact/{id}', 'Front\FrontController@uniquefact');
Route::get('/special-yoga/{id}', 'Front\FrontController@specialYoga');
Route::get('/blog/{slug}', 'Front\FrontController@singleBlog')->where('slug', '[\w\d\-\_]+');
Route::get('/single-page/{slug}', 'Front\FrontController@singlePage')->where('slug', '[\w\d\-\_]+');
Route::get('/single-main-page/{slug}', 'Front\FrontController@singleMainPage')->where('slug', '[\w\d\-\_]+');
Route::get('/single-other/{id}', 'Front\FrontController@singleOther');

Route::get('/fee-list', 'Front\FrontController@feeList');
Route::get('/faqs', 'Front\FrontController@faqS');
Route::get('/testimonial', 'Front\FrontController@testimonial');
//this is apply 
Route::get('/apply/{id}', 'Front\FrontController@applyNow');
Route::post('/apply-now', 'Front\FrontController@applyNowPost');
Route::get('/payment-test', 'Front\FrontController@Paymenttest');
Route::get('/nabil_checkout', 'Front\FrontController@nabil_checkout');
// Route::post('/blog-list', 'Front\FrontController@blogList');



