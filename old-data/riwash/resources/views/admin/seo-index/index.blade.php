@extends('layouts.admin')
@section('content')

     <div class="content">
                        
        
<div class="page-header">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li> Meta Tags For SEO</li>
        </ul>
        @if($informations->isNotEmpty())
        @else
        <ul class="breadcrumb-elements">
            <a href="{{ action('Admin\SeoIndexController@create') }} " class="btn btn-success">Create </a>
        </ul>
        @endif
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a></div>
</div>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        @if($informations->isNotEmpty())
                        <table class="table table-striped">
                    <tr>
                    
                    
                    <th>Auth</th>
                
                    <th>Meta Description</th>
                    <th>Meta Keyword</th>

                   
                    
                   
                    
                    <th>Action</th>
                </tr>
             @foreach($informations as $k => $information)
                <tr>
                    <td>{{$information->auth}}</td>
                    <td>{{$information->des}}</td>
                    <td>{{$information->keyword}}</td>
        
                   
                    
                    
                

                 
                    
                                                            <td>
                                            {{ Form::open(['method' => 'delete', 'action' => ['Admin\SeoIndexController@destroy', $information->id]]) }}
                                            <a href="{{ action('Admin\SeoIndexController@edit', $information->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                    <button type="submit" class="btn btn-danger btn-sm delete" onclick="return confirm('You Want to Delete?');"><i class="fas fa-trash-alt"></i></button>
                                            {{ Form::close() }}
                                        </td>
                </tr>
               
             @endforeach
                </table>
                @else
                <h3>No information Added</h3>
                @endif
            </div>
                </div>
       
      
</div>  
        </div>
    </div>
    </div>  

        
                    </div>



            

        
    

@endsection