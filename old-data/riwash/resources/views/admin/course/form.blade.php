<div class="row">
	<div class="col-sm-6">
<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', null, ['class' => 'form-control']) }}
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
    {{ Form::label('slug', 'slug') }}
    {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Write this way(this-is-slug)']) }}
</div>
</div>


<div class="col-sm-12">
<div class="form-group">
    {{ Form::label('content', 'Content') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'summernote']) }}
</div>

</div>
<div class="col-sm-12">
<div class="form-group">
    {{ Form::label('image', 'Image') }}
    {{ Form::file('image', null, ['class' => 'form-control']) }}
</div>
</div>

<h3>This are for Google SEO:</h3>
<div class="col-sm-6">
<div class="form-group">
    {{ Form::label('des', 'Description') }}
    {{ Form::textarea('des', null, ['class' => 'form-control']) }}
</div>
</div>
<div class="col-sm-6">

<div class="form-group">
    {{ Form::label('keyword', 'keyword') }}
    {{ Form::textarea('keyword', null, ['class' => 'form-control']) }}
</div>

</div>
</div>


<input type="submit" value="Save" class="btn btn-success">