<div class="row">
    <div class="col-sm-6">
<div class="form-group">
    {{ Form::label('main_page_id', 'Main Page') }}
    {{ Form::select('main_page_id', $mainPage ,null, ['class' => 'form-control']) }}
</div>
</div>

	<div class="col-sm-6">
<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', null, ['class' => 'form-control']) }}
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
    {{ Form::label('content', 'Content') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'summernote']) }}
</div>

</div>


</div>


<input type="submit" value="Save" class="btn btn-success">