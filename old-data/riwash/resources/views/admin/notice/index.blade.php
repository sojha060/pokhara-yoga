@extends('layouts.admin')
@section('content')

     <div class="content">
                        
        
<div class="page-header">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li> Notice</li>
        </ul>
        
        <ul class="breadcrumb-elements">
            <a href="{{ action('Admin\NoticeController@create') }} " class="btn btn-success">Create </a>
        </ul>
       
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a></div>
</div>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        @if($informations->isNotEmpty())
                        <table class="table table-striped">
                    <tr>
                    
                    
                    <th>Title</th>
                    <th>Date_for</th>
                    <th>Content</th>
                    <th>image</th>
                   
                    
                   
                    
                    <th>Action</th>
                </tr>
             @foreach($informations as $k => $information)
                <tr>
                    <td>{{$information->title}}</td>
                    <td>{{$information->date_for}}</td>
                    <td>{{$information->content}}</td>
                    <td><img src="{{asset('uploads/'.$information->image)}}" class="img-fluid" style="height: 100px; width: 200px "></td>
                    
                    
                

                 
                    
                                                            <td>
                                            {{ Form::open(['method' => 'delete', 'action' => ['Admin\NoticeController@destroy', $information->id]]) }}
                                            <a href="{{ action('Admin\NoticeController@edit', $information->id) }}" class="btn btn-primary btn-sm">Edit</a>
                    <button type="submit" class="btn btn-danger btn-sm delete" onclick="return confirm('You Want to Delete?');">Delete</button>
                                            {{ Form::close() }}
                                        </td>
                </tr>
               
             @endforeach
                </table>
                @else
                <h3>No information Added</h3>
                @endif
            </div>
                </div>
       
      
</div>  
        </div>
    </div>
    </div>  

        
                    </div>



            

        
    

@endsection