<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('content', 'Content') }}
    {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'summernote']) }}
</div>
<div class="form-group">
    {{ Form::label('image', 'Image') }}
    {{ Form::file('image', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('Date_for', 'Date For') }}
    {{ Form::date('date_for', null, ['class' => 'form-control']) }}
</div>
<input type="submit" value="Add" class="btn btn-success">