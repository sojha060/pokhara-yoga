<div class="form-group">
    {{ Form::label('all_fee_category_id', 'Category') }}
    {{ Form::select('all_fee_category_id',  $categories, null,['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('from', 'From') }}
    {{ Form::text('from', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('to', 'To') }}
    {{ Form::text('to', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('place', 'Place') }}
    {{ Form::text('place', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('share_room', 'Share Room') }}
    {{ Form::text('share_room', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('private_room', 'Private Room') }}
    {{ Form::text('private_room', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">


<input type="submit" value="Save" class="btn btn-success">
</div>