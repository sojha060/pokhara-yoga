@extends('layouts.admin')
@section('content')

     <div class="content">
                        
        
<div class="page-header">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li> All Apply </li>
        </ul>
        
        <ul class="breadcrumb-elements">
            <a href="#" class="btn btn-success">Apply</a>
        </ul>
       
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a></div>
</div>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        @if($informations->isNotEmpty())
                        <table class="table table-striped">
                    <tr>
                    
                   <th> Full Name </th>
                   <th> Email </th>
                   <th> Number </th>
                   <th> Status </th>
                    <th>Created At</th>
                
                    
                   
                    
                    <th>Action</th>
                </tr>
             @foreach($informations as $k => $information)
                <tr>
                  
                   
                    <td>{{$information->name}}</td>
                    <td>{{$information->email}}</td>
                    <td>{{$information->number}}</td>
                    <td class="btn @if($information->status == 'unpaid') btn-danger @else btn-success @endif">{{$information->status}}
                                            </td> 
                    <td>{{ $information->created_at->format('j M, Y h:i A')}}</td>
                  
                   
                    
                    
                    
                

                 
                    
                                                            <td>
                                            {{ Form::open(['method' => 'delete', 'action' => ['Admin\ApplyListController@destroy', $information->id]]) }}
                                            <a href="{{ action('Admin\ApplyListController@show', $information->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            <a href="{{ action('Admin\ApplyListController@edit', $information->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                    <button type="submit" class="btn btn-danger btn-sm delete" onclick="return confirm('You Want to Delete?');"><i class="fas fa-trash-alt"></i></button>
                                            {{ Form::close() }}
                                        </td>
                </tr>
               
             @endforeach
             {{$informations->links()}}
                </table>
                @else
                <h3>No information Added</h3>
                @endif
            </div>
                </div>
       
      
</div>  
        </div>
    </div>
    </div>  

        
                    </div>



            

        
    

@endsection