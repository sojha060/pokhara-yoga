@extends('layouts.admin')
@section('content')

<!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
       
      </h1>
          </section>

    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
       
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            {{$information->course}}
            <strong class="pull-right">Date: {{ $information->created_at->format('j M, Y h:i A')}}</strong>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <h3>Information</h3>
          <address style="font-size: 18px">
            <strong>Name: {{$information->name}}</strong><br>
            <strong>Email: {{$information->email}}</strong><br>
            <strong>Number: {{$information->number}}</strong><br>
            <strong>Location: {{$information->location}}</strong><br>
            <strong>gender: {{$information->gender}}</strong><br>
            
          
          </address>
        </div>
        
        
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead style="font-size: 16px;">
            <tr>
              <th>Category</th>
              <th>Course</th>
              <th>Date</th>
              <th>Place</th>
              <th>Room Type</th>
              
            </tr>
            </thead>
            <tbody style="font-size: 16px;">
            <tr>
              <td>{{$information->allFeeList->allFeeCategory->name}}</td>
              <td>{{$information->course}}</td>
              <td>{{$information->allFeeList->from}} - {{$information->allFeeList->to}} </td>
              <td>{{$information->allFeeList->place}}</td>
              <td>{{$information->room_type}}</td>
             
            </tr>
           
            
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6 ">
          <p class="lead">Status: <span class="btn @if($information->status == 'unpaid') btn-danger @else btn-success @endif">{{$information->status}} </span></p>
          

          
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
        	@php
        	$share = $information->allFeeList->share_room;
        	$private = $information->allFeeList->private_room;

        	 @endphp


          <p class="lead">Total Amount: @if($information->room_type == 'share_room')  {{$share}} @else {{$private}}  @endif </p>

         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          {{-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> --}}
          
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

  <!-- /.content-wrapper -->







@endsection