

@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")
 @section("contact")
  @endsection
@include('front.includes.header')

<!-- start banner Area -->
           <div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1 class="text-uppercase">Contact Us</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Contact Us</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
            <!-- End banner Area -->   
                           {{--  --}}
                           <div class="contact-body">
  <div class="container">
    <div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
    <div class="row">
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 contactbox">
        <i class="fa fa-map-marker"></i>
        <h4>Address</h4>
        <p>Leak Side Road Sedi hight  </p>
        <p>Pokhara, Nepal</p>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 contactbox">
        <i class="fa fa-phone"></i>
        <h4>Phone</h4>
        <p>+977-9856027660</p>
        <p>+977-6-1420115</p>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 contactbox">
        <i class="fa fa-envelope"></i>
        <h4>Email Address</h4>
        <p><a href="mailto:Contact@pokharayogaschoolandretreatcenter.com">Contact@pokharayogaschoolandretreatcenter.com </a></p>
        <p><a href="mailto:info@pokharayogaschoolandretreatcenter.com">Info@pokharayogaschoolandretreatcenter.com </a></p>
      </div>
    </div>
  </div>
</div>
<div class="contact-message">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Send Us Message</h4>

                                @if($errors->any())
            <div class = 'alert alert-danger'>
                <ul>
                    @foreach($errors->all() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(\Session::has('msg'))
            <div class = 'alert alert-success'>
                <p>{{ \Session::get('msg') }}</p>
            </div></br>
            @endif
        <form action="{{action('Front\FrontController@contactUsPost')}}" method="POST">
          <div class="row">
            <div class="col-md-6">
              <div class="md-form">
                <input type="text" name="name" class="form-control" placeholder="Your name">
              </div>
              <div class="md-form">
                <input type="email" name="email" class="form-control" placeholder="Your email">
              </div>
              <div class="md-form">
                <input type="text" name="number" class="form-control" placeholder="Phone Numaber">
              </div>
              <div class="md-form">
                <input type="text" name="subject" class="form-control" placeholder="Subject">
              </div>
            </div>
            <div class="col-md-6">
              <div class="md-form">
                <textarea type="text" name="message" placeholder="Your message"></textarea>
              </div>
              @csrf
              <div class="form-group pt-1">
              <input type="submit" value="Send" class="btn btn-success send-btn text-white pt-2 pb-3 text-uppercase" style="font-size: 20px">
          </div>
             
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="maps">
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14061.500755413554!2d83.9579024!3d28.2262898!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x66a4a983cbaa31f8!2sInfinity%20Resort!5e0!3m2!1sen!2sde!4v1582266869455!5m2!1sen!2sde" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>








                       
            

@include('front.includes.footer')
