 @section("title", "$information->title")
  @section("desc", "$information->des")
  @section("keyword", "$information->keyword")
 @section("singleblog")
  @endsection
@include('front.includes.header')
<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>{{$information->title}}</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>{{$information->title}}</li>
          
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="detail-body ">
  <div class="container">
    <div class="social-icons pb-3">
  @include('front.includes.social-media')
</div>
    <div class="row">
      <div class="col-12 col-sm-12 col-md-8 col-lg-9 detail-left">
        <img src="{{asset('uploads/'.$information->image)}}" alt="">
        <h4>{{$information->title}}</h4>
        <p>{!! $information->content !!}</p>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-3">
        <div class="row">
          
          @include('front.includes.blog-sidebar')



        </div>
      </div>
    </div>
  </div>
</div>







<div class="mt-5"></div>
@include('front.includes.footer')