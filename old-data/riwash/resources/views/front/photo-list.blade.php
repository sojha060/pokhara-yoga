
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")@section("photolist")
  @endsection
@include('front.includes.header')


  
			<!-- start banner Area -->
			<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Gallery</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Gallery</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
			<!-- End banner Area -->	
				
			<!-- Start gallery Area -->
			
			<section class="gallery-area section-gap mt-5 mb-5">
				<div class="container">
					<div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
					<div class="row">
						
						@foreach($photoList as $list)
						<div class="col-lg-4">
							<div class="box">    
							<a href="{{asset('uploads/'.$list->image)}}" class="img-gal link-gallery" data-lightbox="roadtrip">
								<div class="single-imgs relative">		
									<div class="overlay overlay-bg"></div>
									<div class="relative">					
										<img class="img-fluid img-gallery modal-img pt-3" src="{{asset('uploads/'.$list->image)}}" alt="" style="height: 250px">				
									</div>
								</div>
							</a>
						</div>
					</div>
				
						@endforeach
						
						
					</div>
				</div>	
			</section>
			<!-- End gallery Area -->
													

			










@include('front.includes.footer')