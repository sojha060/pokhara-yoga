
@section("title", "Pokhara Yoga School The Best Yoga School In Nepal . Best yoga school in pokhara nepal")
@section("keyword", "pokhara yoga school , 300 hour yoga teacher training in nepal , yoga school in pokhara nepal,  best yoga school nepal,300 hour yoga teacher training in pokhara nepal,yoga training courses nepal, yoga school in nepal,best yoga school in nepal,500 hour yoga teacher training nepal,nepal school of yoga, 500 hour yoga teacher training in pokhara nepal, yoga teacher training nepal 2019, best yoga school in pokhara nepal, 200 hour yoga teacher training in pokhara nepal, yoga teacher training nepal 2020,200 hours yoga teacher training nepal ,yoga training nepal,best yoga school in nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is the best yoga school in pokhara nepal 2020 || best yoga school in nepal.")

 @section("home")
  @endsection
 @include('front.includes.header')
 
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "Pokhara yoga School",
  "url": "http://pokharayogaschoolandretreatcenter.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://pokharayogaschoolandretreatcenter.com/search?q={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<div class="slider">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <li data-target="#carouselExampleCaptions" data-slide-to="{{$c}}" class="@if($c == 1) active @else @endif"></li>
      @php $c++; @endphp
      
     @endforeach
    </ol>
    <div class="carousel-inner">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <div class="carousel-item @if($c == 1) active @endif">
        <img src="{{asset('uploads/'.$slider->image)}}" class="d-block w-100" alt="Yoga school in nepal">
        <div class="carousel-caption d-none d-md-block">
          <h5>{{$slider->title}} </h5>
          <p>{!! $slider->content !!}</p>
         {{--  <a href="#" class="btn btn-banner1">200hr</a>
          <a href="#" class="btn btn-banner2">300hr</a>
          <a href="#" class="btn btn-banner3">400hr</a>
          <a href="#" class="btn btn-book">Book Now</a> --}}
        </div>
      </div>
      @php $c++; @endphp
      
     @endforeach
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a> -->
  </div>
</div>




@if($yogaTraningNepal->isEmpty())
@else
<div class="teaching-section">
  <div class="container">
    <dive class="row">
      <div class="col-12 col-sm-12">
        <h3>Yoga Teacher Training In Nepal</h3>
      </div>
      @foreach($yogaTraningNepal as $item)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
        <img src="{{asset('uploads/'.$item->image)}}" alt="Yoga school in nepal">
        <div class="teach-box">
          <h1>{{$item->title}}</h1>
          <p>{{ str_limit(strip_tags($item->content), 250) }}</p>
          <a href="{{action('Front\FrontController@yogaTrainingNepal',$item->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
  
</div>
@endif

<div class="about-section">
  <div class="container">
    <div class="row">
      @foreach($aboutUs as $about)
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
        <div class="about-box">
          <h4>{{$about->title}}</h4>
          <p class="text-justify">
          {!! $about->content !!}
        </p>
        </div>
      </div>
      @endforeach
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
        <div class="video-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="video-box">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>      </div>
      </div>
    </div>
  </div>
</div>


<section id="tabs" >
  
  
                        @if($errors->any())
            <div class = 'alert alert-danger'>
                <ul>
                    @foreach($errors->all() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(\Session::has('msg'))
            <div class = 'alert alert-success'>
                <p>{{ \Session::get('msg') }}</p>
                <script> alert('{{ \Session::get('msg') }}'); </script>
            </div></br>
            @endif
  <div class="container">
    <h6 class="section-title h1">Yoga Teacher Training Fees and Schedule</h6>
    <div class="row">
      <div class="col-12 col-sm-12">
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            @php $c = 1; @endphp
            @foreach($feeList as $category)
            <a class="nav-item nav-link  @if($c == 1) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home{{$category->id}}" role="tab" aria-controls="nav-home{{$category->id}}" aria-selected="true">{{$category->name}}</a>

@php $c++ @endphp
             @endforeach
          </div>
        </nav>
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
       
     
 @php $c = 1 @endphp
@foreach($feeList as $category)


          <div class="tab-pane fade show @if($c == 1) active @endif " id="nav-home{{$category->id}}" role="tabpanel" aria-labelledby="nav-home-tab{{$category->id}}">
     <h5>{{$category->title}}</h5>
            <div class="table-responsive" id="sailorTableArea">
              <table id="sailorTable" class="table table-striped table-bordered" width="100%" style="color:#fff">
                <thead>
                  <tr>
                    <th scope="col">From</th>
                    <th scope="col">To</th>
                    <th scope="col">Place</th>
                    <th scope="col">Share Room</th>
                    <th scope="col">Private Room</th>
                    <th scope="col">Enquiry</th>
                  </tr>
                </thead>
                <tbody>
                   
                  @foreach($category->allFeeList->sortBy('created_at') as $list)
                    <tr>
                      <td>{{$list->from}}</td>
                      <td>{{$list->to}}</td>
                      <td>{{$list->place}}</td>
                      <td>{{$list->share_room}} USD</td>
                      <td>{{$list->private_room}} USD</td>
                      <td><a href="{{action('Front\FrontController@applyNow',$list->id)}}" class="apply">  Apply <img src="{{ asset('uploads/new1.gif') }}" alt="Yoga school in nepal"></a></td>
                    </tr>
                     @endforeach
                   
                   
                    
                </tbody>
              </table>
            </div>
          </div>
@php $c++ @endphp
         
          @endforeach





         
   
        </div>
      
      </div>
      <div class="col-12 col-sm-12">
      
      </div>
    </div>
  </div>
</section>



@if(!$ourTeam->isEmpty())
<div class="yoga-teacher">
  <div class="container px-5">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Our Yoga Teachers</h4>
        <p>For quite a long while, our particularly talented teachers have been controlling and affecting the understudies to turn into the leader of their own lives and to stir their own inward holiness. The yogic way of thinking, engaging encounters, and monstrous information on our instructors will urge you to make an effective change in your locale.</p>
      </div>
      @foreach($ourTeam as $team)
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
        <img src="{{asset('uploads/'.$team->image)}}" alt="Yoga school in nepal"  class="team-img">
        <h6>{{$team->name}}</h6>
        <a href="{{action('Front\FrontController@teacher',$team->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
      </div>
      @endforeach
      
      
      <div class="col-12 col-sm-12">
        <a href="#" class="btn btn-teacher wow shake" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: shake;">View More Teacher <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif




{{-- <section id="tabs">
  <div class="container">
    <h6 class="section-title h1">Yoga Teacher Training Upcoming Events</h6>
    <div class="row">
      <div class="col-12 col-sm-12">
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                @php $c = 1 @endphp
               @foreach($events as $event)
            <a class="nav-item nav-link @if($c == 1) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home{{$event->id}}" role="tab" aria-controls="nav-home{{$event->id}}" aria-selected="true">{{$event->name}}</a>
            @php $c++ @endphp
            @endforeach

           <!--  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Kerela</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Nepal</a> -->
          </div>
        </nav>
   
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          @php $c = 1 @endphp
        @foreach($events as $event)

          <div class="tab-pane fade show @if($c == 1) active @endif" id="nav-home{{$event->id}}" role="tabpanel" aria-labelledby="nav-home-tab{{$event->id}}">
            <div class="table-responsive" id="sailorTableArea">
              <table id="sailorTable" class="table table-striped table-bordered" width="100%">
                <tbody>
                  @foreach($event->upcommingEventList as $eventList)
                    <tr>
                        <td>{{$eventList->title}}</td>
                        <td>{{$eventList->date}}</td>
                        <td>{{$eventList->price}}</td>
                    </tr>
                    @endforeach
                    
                    
                </tbody>
              </table>
            </div>
          </div>
          @php $c++ @endphp

          @endforeach
        </div>


      
      </div>
    </div>
  </div>
</section> --}}
@if(!$specialOffer->isEmpty())
<div class="special">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Special Offers for Yoga Teacher Training</h4>
      </div>
      @foreach($specialOffer as $list)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$list->image)}}" alt="Yoga school in nepal">
        <a href="#" class="btn btn-offer">Book Now <i class="fa fa-angle-double-right"></i></a>
      </div>
      @endforeach
      
    </div>
  </div>
</div>
@else
@endif

@if($otherInformation->isEmpty())
@else
<div class="teaching-section">
  <div class="container">
    <dive class="row">
      <div class="col-12 col-sm-12">
        <h3>Yoga Teacher Training In Nepal</h3>
      </div>

      @foreach($otherInformation as $other)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft; ">
        <img src="{{asset('uploads/'.$other->image)}}" alt="Yoga school in nepal" style="height:300px">
        <div class="teach-box">
          <h4>{{$other->title}}</h4>
          <p>{{ str_limit(strip_tags($other->content), 200) }}</p>
          <a href="{{action('Front\FrontController@singleOther',$other->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
  
</div>
@endif





@if(!$uniqueFact->isEmpty())
<div class="program-section">
  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-12">
        <h4>The Unique Facets of our Yoga Teacher Training Programs</h4>
      </div>
       @foreach($uniqueFact as $fact)

      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$fact->image)}}" alt="Yoga school in nepal">
        <div class="teach-box">
          <h4>{{$fact->title}}</h4>
          <p>{!! str_limit($fact->content, 300) !!}</p>
          <a href="{{action('Front\FrontController@uniquefact',$fact->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
 @endforeach
    
     
    </div>
  </div>
</div>
@else
@endif


@if(!$testimonials->isEmpty())
<div class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Yoga Teacher Training Student Testimonials</h4>
      </div>
      @foreach($testimonials as $test)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <div class="testi-box">
          <img src="{{asset('uploads/'.$test->image)}}" alt="Yoga school in nepal">
          <h6><i class="fa fa-quote-left"></i> {{$test->name}}</h6>
          <p>{!! $test->content !!}</p>
          <p><strong>Thank You</strong></p>
        </div>
      </div>
      @endforeach

    
      <div class="col-12 col-sm-12">
        <a href="{{action('Front\FrontController@testimonial')}}" class="btn btn-testi">Read More Testimonials <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif

@if(!$yogaCourse->isEmpty())
<div class="teaching-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Special Yoga Teacher Training Courses</h3>
      </div>
      @foreach($yogaCourse as $course)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$course->image)}}" alt="Yoga school in nepal">
        <div class="teach-box">
          <h4>{{$course->title}}</h4>
          <p>{!! str_limit($course->content, 250) !!}</p><br>
          <a href="{{action('Front\FrontController@specialYoga',$course->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach
    </div>
  </div>
</div>
@else
@endif

<div class="teacher-training">
  <div class="container">
    <div class="row">
      @foreach($whyYoga as $why)
      <div class="col-12 col-sm-12 wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
        <div class="tech-box">
          <h4>{{$why->title}}</h4>
          <p>{!!  $why->content !!}</p>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

{{-- this is for gallery --}}
<section>
  <div class="container">
    <h3 class="text-center pt-3">Our Gallery</h3>
    <div class="row">
      {{-- single col --}}
      @foreach($photoList as $photo)
      <div class="col-sm-3 py-3">
        <a href="{{asset('uploads/'.$list->image)}}" class="img-gal link-gallery" data-lightbox="roadtrip">
        <div class="card photo-list">
          <img src="{{ asset('uploads/'.$photo->image) }}" alt="Pokhara yoga school" class="w-100 img-fluid" >
        </div>
      </a>

      </div>
      @endforeach
      {{-- /single col --}}

    </div>

  </div>
  
</section>







{{-- <div class="training-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4><a href="#">200 Hour Yoga Teacher Training in Pokhara Nepal 2020</a></h4>
        <p class="hour">200 hour yoga teacher training in Pokhara Nepal by yoga alliance certified school. </p>
      </div>
      @foreach($hourYoga as $hour)
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        
        <div class="class-box">
          <h6><i class="fas fa-hand-point-right wow shake" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: shake;" style="color: #7d491a; margin-right: 5px;"></i> {{$hour->title}} <span>{{$hour->date}}</span></h6>
          <p>{!! $hour->content !!}<p> 

        </div>
      
        
      </div>
      @endforeach
    </div>
  </div>
</div> --}}

<div class="teaching-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Our Blogs</h3>
      </div>
      @foreach($blogs as $blog)
      <div class="col-sm-4">
        @if(!is_null($blog->image))
        <img src="{{asset('uploads/'.$blog->image)}}" alt="Yoga school in nepal">
        @else
        @endif

        <div class="teach-box">
          <h4>{{$blog->title}}</h4>
          <ul>
            <li><i class="fa fa-user"></i> by <a href="#">Admin</a></li>
            <li><i class="fa fa-calendar"></i>{{ $blog->created_at->format('j M, Y') }}</li>
          </ul>
          <p>{{ strip_tags(str_limit($blog->content, 200)) }}</p>
          <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach

    </div>
  </div>
</div>


@include('front.includes.footer')