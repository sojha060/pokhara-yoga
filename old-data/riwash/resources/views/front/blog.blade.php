  @foreach($aboutUs as $about)
 
  @section("title", "$about->title")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")
 @section("about")
  @endsection
  @endforeach
@include('front.includes.header')

 


<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>About Us</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>About Us</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="teaching-section py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Our Blogs</h3>
      </div>
      @foreach($blogs as $blog)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
      

        <div class="teach-box">
          <h4>{{$blog->title}}</h4>
          <ul>
            <li><i class="fa fa-user"></i> by <a href="#">Admin</a></li>
            <li><i class="fa fa-calendar"></i>{{ $blog->created_at->format('j M, Y') }}</li>
          </ul>
          <p>{!! str_limit($blog->content, 200) !!}</p>
          <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach

    </div>
  </div>
</div>
@else
@endif






@include('front.includes.footer')