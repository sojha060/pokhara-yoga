<div class="posts">
            <div class="col-12 col-sm-12">
              <h4>Recent Posts</h4>
            </div>
            @foreach($blogs as $blog)
            <div class="col-12 col-sm-12">
              <div class="recent-post">
                <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}"><img src="{{asset('uploads/'.$blog->image)}}" alt=""> <h6>{{$blog->title}}</h6></a>
                <ul>
                  <li><i class="fa fa-user"></i> By: <a href="#">Admin</a></li>
                  <li><i class="fa fa-calendar"></i> {{$blog->created_at->format('j M, Y')}}</li>
                </ul>
              </div>
            </div>
            
           @endforeach
           {{$blogs->links()}}
          </div>
