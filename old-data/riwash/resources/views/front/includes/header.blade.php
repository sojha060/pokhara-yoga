   <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
   <html lang="en">
   <head>
    
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   @foreach($logos as $logo)
   @foreach($metaTags as $tag)
    <title> @yield('title')</title>
   
    <meta property="og:url"           content="{{url()->current()}}" />
  <meta property="og:type"          content="Pokhara Yoga School" />
  <meta property="og:title"         content=" @yield('title')" />
  <meta property="og:description"   content=" @yield('desc')" />
  <meta property="og:image" content="{{ asset('uploads/'.$logo->logo) }}" />
  <meta name="author" content="{{ $tag->auth }}">
  <meta name="description" content="@yield('desc')">
  <meta name="keywords" content=" @yield('keyword')">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   @endforeach
   @endforeach
   <link rel="canonical" href="https://pokharayogaschoolandretreatcenter.com" hreflang="en">
   <meta name="google-site-verification" content="Oz-mSFiSV1mePMCUtC3CtHSRN177F0hZpB-zWTZZ9qo" />
    <!--Bootstrap CSS-->
    <link rel="icon" href="{{ asset('uploads/'.$logo->logo) }}" type="image/gif" sizes="16x16" hreflang="en">

    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}" hreflang="en">

    <!--Font Awesome CSS-->
  {{--   <link rel="stylesheet" href="{{asset('front/css/all.css')}}" hreflang="en"> --}}

    <!--Main CSS-->
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}" hreflang="en">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" hreflang="en">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" hreflang="en">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet" hreflang="en">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet" hreflang="en">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.css" hreflang="en">

    <!--Owl Carousel-->
    <link rel="stylesheet" href="{{asset('front/css/owl.carousel.min.css')}}" hreflang="en">
    <link rel="stylesheet" href="{{asset('front/css/owl.theme.default.min.css')}}" hreflang="en">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158891144-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158891144-1');
</script>



</head>
<body>
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>

<header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <div class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 header-left">
          <ul>
            <li><a href="#"><i class="fa fa-phone-volume"></i> +977 9856027660</a></li>
            <li><a href="mailto:Contact@pokharayogaschoolandretreatcenter.com"><i class="fa fa-envelope"></i> Contact@pokharayogaschoolandretreatcenter.com</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 header-right">
<span id="google_translate_element" > </span>
          <ul class="wow shake" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: shake;">

            <li><a href="{{ action('Front\FrontController@contactUs') }}" class="applys" hreflang="en">Contact Now <i class="fas fa-pen-square"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="menu-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 ">
        <nav class="navbar navbar-expand-lg navbar-light">
   @foreach($logos as $logo)

          <a class="navbar-brand" href="{{action('Front\FrontController@index')}}"><img src="{{asset('uploads/'.$logo->logo)}}" alt=""></a>
          @endforeach

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{action('Front\FrontController@index')}}">Home</a>
              </li>
             @foreach($mainPage as $page)
             
              <li class="nav-item dropdown ">
                <a class="nav-link" href="{{action('Front\FrontController@singleMainPage',$page->slug)}}">{{$page->title}}</a>
                <ul>
                  @foreach($page->subPage->sortBy('order') as $subPage)
                  <li><a href="{{action('Front\FrontController@singlePage',$subPage->slug)}}">{{$subPage->title}}</a></li>
                  @endforeach
                  
                </ul>
              </li>


              
              @endforeach
               <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@photoList')}}">Gallery</a>
              </li>
              <!-- <li class="nav-item ">-->
              <!--  <a class="nav-link" href="{{action('Front\FrontController@blog')}}">Blog </a>-->
              <!--</li>-->
              <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@contactUs')}}">Contact Us</a>
              </li>
            </ul>
          </div>
        </nav>
        <div class="mobile-header">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-12">
              <span class="clickmenus" onclick="openNav()">&#9776; </span>
              <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="mobile-menus">
                  <ul>
                    <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
                    @foreach($mainPage as $page)
                    <li>
                      <a href="{{action('Front\FrontController@singleMainPage',$page->slug)}}"  @if(!$page->subPage->isEmpty()) type="text" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2" @endif >{{$page->title}}  @if(!$page->subPage->isEmpty()) <i class="wsmenu-arrow fa fa-angle-down "></i> @endif </a>
                      <div class="collapse multi-collapse" id="multiCollapseExample2">
                        <div class="card card-body">
                        <ul>
                          @foreach($page->subPage->sortBy('order') as $subPage)
                  <li><a href="{{action('Front\FrontController@singlePage',$subPage->slug)}}">{{$subPage->title}}</a></li>
                  @endforeach
                        
                        </ul>
                      </div>
                    </li>
                    @endforeach
                    
                    
                  </ul>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="col-12 col-sm-12 col-md-2 col-lg-2">
        <div class="menu-adds">
          {{-- <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p> --}}
          {{-- <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
      <div class="col-12 col-sm-12">
        <div class="menu-adds-mobile">
         {{--  <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p> --}}
         {{--  <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
    </div>
  </div>
</header>