  @foreach($aboutUs as $about)
 
  @section("title", "$about->title")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")
 @section("about")
  @endsection
  @endforeach
@include('front.includes.header')

 


<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>About Us</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>About Us</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>



@foreach($aboutUs as $about)
 <div class="about-body">
  <div class="container">
    <div class="social-icons">
  @include('front.includes.social-media')
</div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <p>{!!  $about->content !!}</p>
        <h4>NAMASTE!</h4>
      </div>
    </div>
  </div>
</div>
@endforeach




@include('front.includes.footer')