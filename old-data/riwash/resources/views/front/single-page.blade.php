
@if(is_null($information->keyword))
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")


  @else
  
   @section("title", "$information->title")
  @section("desc", "$information->des")

  @section("keyword", "$information->keyword")

@endif


 @section("singlepage")
  @endsection
@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>{{$information->title}}</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>{{$information->title}}</li>
         

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="about-body text-justify">
  <div class="container">
  
          

    <div class="row">
     
      <div class="col-12 col-sm-12  col-md-8 col-lg-8  single-page">
        @if($information->name)
        <h1> <strong>Name:</strong><span class="pr-1"> </span>{{$information->name}}</h1>
@else 
@endif
        <p>{!!  $information->content !!}</p>

        
      </div>

   
   <div class="col-sm-4">
    <div class="card py-2 px-2">
    <h3 class="py-2" style="color: #026B2F">Service Hightlight</h3>
    <p> <div class="fb-page" data-href="https://www.facebook.com/pokharayogaschool/" data-tabs="timeline" data-width="" data-height="320px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pokharayogaschool/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pokharayogaschool/">Pokhra Yoga School</a></blockquote></div></p>
  </div>
   
   </div> 
   
    </div>
  </div>
</div>

<style type="text/css">
  
.faqs-body{
 padding: 0px;
}
</style>


<div class="faqs-body">
  <div class="container">
    <div class="social-icons">
</div>
    <div class="row">
      <div class="col-12 col-sm-12">

      </div>

@if(is_null($information->mainPageDrop))
   @else
      @foreach($information->mainPageDrop as $drop)
      <div class="col-12 col-sm-12 col-sm-6 col-md-6">
        <div class="accordion-container">
          <div class="set">
            <a href="javascript:void(0)">
              {{ $drop->title }}
              <i class="fa fa-plus"></i>
            </a>
            <div class="content" style="display: none;">
                <p>{!! $drop->content !!}</p>
            </div>
          </div>

         
         
        </div>
      </div>
      @endforeach
   
      @endif
     


      
    </div>
  </div>
</div>





@include('front.includes.footer')