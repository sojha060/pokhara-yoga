
 
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.") @section("apply")

  @endsection

@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Apply Now</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Apply Now</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container py-5">

<div class="card" >
  <form action="{{action('Front\FrontController@applyNowPost')}}" method="POST" class="form-group"> 
                        @if($errors->any())
            <div class = 'alert alert-danger'>
                <ul>
                    @foreach($errors->all() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(\Session::has('msg'))
            <div class = 'alert alert-success'>
                <p>{{ \Session::get('msg') }}</p>
                <script> alert('{{ \Session::get('msg') }}'); </script>
            </div></br>
            @endif
    <div class="row px-5 py-5">
      <div class="col-sm-12">
        <h5 class="text-center pb-3">Apply For This Things</h5>
      </div>
    {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="course">Course</label>
        <input type="text" value="{{$information->allFeeCategory->title}}" name="course" placeholder="{{$information->allFeeCategory->title}}" class="form-control" readonly>
      </div>
    </div>
    {{-- /single  --}}
    {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="date">Date</label>
        <input type="text"  name="date" placeholder="{{$information->from}} - {{$information->to}}" class="form-control" disabled="disabled">
      </div>
    </div>
    {{-- /single  --}}
      {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="date">Place</label>
        <input type="text"  name="place" placeholder="{{$information->place}}"  class="form-control" disabled="disabled">
      </div>
    </div>
    {{-- /single  --}}



   {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="room_type">Choose Room</label>
        <select name="room_type" class="form-control">
          <option value="share_room">Share Room</option>
          <option value="Private_room">Private Room</option>
         

        </select>
      </div>
    </div>
    {{-- /single  --}}
     {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="name">Full Name</label>
        <input type="text" name="name" class="form-control">
      </div>
    </div>
    {{-- /single  --}}
    {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="gender">Gender</label>
        <select name="gender" class="form-control">
          <option value="male">Male</option>
          <option value="female">Female</option>
         

        </select>
      </div>
    </div>
    {{-- /single  --}}

     {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control">
      </div>
    </div>
    {{-- /single  --}}

       {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="number">Number</label>
        <input type="text" name="number" class="form-control">
      </div>
    </div>
    {{-- /single  --}}

      {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="location">Your Location</label>
        <input type="text" name="location" class="form-control">
      </div>
    </div>
    {{-- /single  --}}
        {{-- single  --}}
    <div class="col-sm-6">
      <div class="form-group">
        <label for="location"><span class="text-white">. </span></label>
 <input type="hidden" name="all_fee_list_id" value="{{$information->id}}" >
      @csrf
  <input type="submit" value="Apply" class="btn btn-info form-control">
      </div>
    </div>
    {{-- /single  --}}



</div>


  </form>

</div>

</div>

















@include('front.includes.footer')
