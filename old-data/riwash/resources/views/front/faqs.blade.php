
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")
 @section("faqs")
  @endsection
@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>FAQS</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>FAQS</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- faqs --}}


<div class="faqs-body">
  <div class="container">
    <div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Frequently Asked Questions</h4>
      </div>


      @foreach($faqS as $list)
      <div class="col-12 col-sm-12 col-sm-6 col-md-6">
        <div class="accordion-container">
          <div class="set">
            <a href="javascript:void(0)">
             {{ $list->title }}
              <i class="fa fa-plus"></i>
            </a>
            <div class="content" style="display: none;">
                <p>{!! $list->content !!}</p>
            </div>
          </div>

         
         
        </div>
      </div>
      @endforeach
     


      
    </div>
  </div>
</div>













@include('front.includes.footer')
