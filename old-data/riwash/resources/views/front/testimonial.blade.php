
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")

  @section("desc", "Our Testimonial")
 @section("test")
  @endsection
@include('front.includes.header')
<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Testimonial</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Testimonial</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>





<div class="testimonials">
  <div class="container">
    <div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Yoga Teacher Training Student Testimonials</h4>
      </div>
      @foreach($testimonials as $test)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <div class="testi-box">
          <img src="{{asset('uploads/'.$test->image)}}" alt="">
          <h6><i class="fa fa-quote-left"></i> {{$test->name}}</h6>
          <p>{!! $test->content !!}</p>
          <p><strong>Thank You</strong></p>
        </div>
      </div>
      @endforeach

    
      </div>
    </div>
  </div>
</div>

@include('front.includes.footer')
