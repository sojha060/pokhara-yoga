
@section("title", "Pokhara Yoga School And Retreat Center Is The Best Yoga School In Nepal")
@section("keyword", "pokhara yoga school , Yoga school in Pokhara Nepal , yoga school in pokhara,nepal best yoga school nepal,earth yoga school nepal,yoga training courses nepal,yoga school in nepal,best yoga school in nepal,nepal school of yoga lalitpur,nepal school of yoga,yoga teacher training nepal 2019,yoga teacher training nepal 2020,yoga teacher training nepal 2018 ,yoga training nepal ,yoga certification nepal,yoga school kathmandu")
@section("desc", "Pokhara Yoga School is best   yoga school in 2020 Nepal with verified and professional yoga Teachers.")
 @section("feelist")
  @endsection
@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>Fee</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>Fee</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
{{--  --}}


<section id="tabs" >
  
  
                        @if($errors->any())
            <div class = 'alert alert-danger'>
                <ul>
                    @foreach($errors->all() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(\Session::has('msg'))
            <div class = 'alert alert-success'>
                <p>{{ \Session::get('msg') }}</p>
                <script> alert('{{ \Session::get('msg') }}'); </script>
            </div></br>
            @endif
  <div class="container">
    <h6 class="section-title h1">Yoga Teacher Training Fees and Schedule</h6>
    <div class="row">
      <div class="col-12 col-sm-12">
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          	@php $c = 1; @endphp
          	@foreach($feeList as $category)
            <a class="nav-item nav-link  @if($c == 1) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home{{$category->id}}" role="tab" aria-controls="nav-home{{$category->id}}" aria-selected="true">{{$category->name}}</a>

@php $c++ @endphp
             @endforeach
          </div>
        </nav>
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
       
     
 @php $c = 1 @endphp
@foreach($feeList as $category)


          <div class="tab-pane fade show @if($c == 1) active @endif " id="nav-home{{$category->id}}" role="tabpanel" aria-labelledby="nav-home-tab{{$category->id}}">
     <h5>{{$category->title}}</h5>
            <div class="table-responsive" id="sailorTableArea">
              <table id="sailorTable" class="table table-striped table-bordered" width="100%" style="color:#fff">
                <thead>
                  <tr>
                    <th scope="col">From</th>
                    <th scope="col">To</th>
                    <th scope="col">Place</th>
                    <th scope="col">Share Room</th>
                    <th scope="col">Private Room</th>
                    <th scope="col">Enquiry</th>
                  </tr>
                </thead>
                <tbody>
                	 
                	@foreach($category->allFeeList as $list)
                    <tr>
                      <td>{{$list->from}}</td>
                      <td>{{$list->to}}</td>
                      <td>{{$list->place}}</td>
                      <td>{{$list->share_room}}</td>
                      <td>{{$list->private_room}}</td>
                      <td><a href="{{action('Front\FrontController@applyNow',$list->id)}}" class="apply">Apply</a></td>
                    </tr>
                     @endforeach
                   
                   
                    
                </tbody>
              </table>
            </div>
          </div>
@php $c++ @endphp
         
          @endforeach





         
   
        </div>
      
      </div>
      <div class="col-12 col-sm-12">
      
      </div>
    </div>
  </div>
</section>








@include('front.includes.footer')