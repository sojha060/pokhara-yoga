<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllFeeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_fee_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('from');
            $table->string('to');
            $table->string('place');
            $table->string('share_room');
            $table->string('private_room');
            $table->bigInteger('all_fee_category_id')->index()->unsigned();
            $table->timestamps();
            $table->foreign('all_fee_category_id')
            ->references('id')
            ->on('all_fee_categories')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_fee_lists');
    }
}
