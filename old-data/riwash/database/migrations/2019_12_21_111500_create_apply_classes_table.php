<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplyClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->string('course');
            $table->string('location');
            $table->string('gender');
            $table->string('room_type');
            $table->string('status')->defult('unpaid');
            $table->bigInteger('all_fee_list_id')->index()->unsigned();
            $table->timestamps();
            $table->foreign('all_fee_list_id')
            ->references('id')
            ->on('all_fee_lists')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply_classes');
    }
}
