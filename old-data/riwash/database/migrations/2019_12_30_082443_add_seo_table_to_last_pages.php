<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoTableToLastPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('last_pages', function (Blueprint $table) {
           $table->bigInteger('order');
            $table->text('des');
            $table->text('keyword');
            $table->string('slug');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('last_pages', function (Blueprint $table) {
            $table->dropColumn('order', 'des', 'keyword', 'slug');
            
        });
    }
}
