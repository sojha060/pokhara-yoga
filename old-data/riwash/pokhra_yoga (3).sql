-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2019 at 08:45 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokhra_yoga`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(2, 'About Rishikul Yogshala', '', '<p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins;\">Rishikul Yogshala, founded in 2010, is honored to serve its students with the spiritual wisdom that goes beyond yoga as a tradition or asana practice. It will guide you to harmonize your higher self, inner voice, and your soul\'s innermost desire.</p><p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins;\">Our approach is unique as we apply the ancient yogic traditions to the radically different life of the 21st century. From the spiritual practices of yoga to breathing techniques, meditation, cleansing practices and more, our meticulously crafted training programs offer you with a sense of heightened awareness. With our yoga teacher training courses in India and also in other parts of the globe, we are focused on forging a path committed towards spiritual integrity.</p><p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins;\">The transformative programs and various types of retreats will provide you with influential and life-changing experiences. Our highly experienced teachers will guide you on your journey of self-discovery by devoting every bit of their experience and knowledge.</p><p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins;\">The family at Rishikul Yogshala will always make sure that you feel like at home, away from home, while you deepen your practice and expand your awareness. Bring your unabashed, true self to us and we would take the responsibility to guide you towards your highest potential.</p>', '2019-12-12 09:17:46', '2019-12-18 01:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `all_fee_categories`
--

CREATE TABLE `all_fee_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `all_fee_categories`
--

INSERT INTO `all_fee_categories` (`id`, `name`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, '200hr YTTC', '200 Hour Yoga Teacher Training In Rishikesh Fee & Schedule 2020', '<p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins; background-color: rgb(1, 133, 58);\">Focus, patience, and perseverance combined with a bit of an investment in terms of time, money, and effort – this is all it takes to climb the wonderful ladder of eternal mental and physical transformation. A yoga teacher training certification at Rishikul Yogshala is the commencement of an evolution --from an ordinary human being to an accomplished teacher and a true Yogi.</p><p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins; background-color: rgb(1, 133, 58);\">Whether you are a self-practitioner willing to dig deeper into the theory and practice of the art or a curious soul aiming to unravel the mystics of this unparalleled science, the road starts right here with our finest Yoga Teacher Training courses.</p>', '2019-12-17 02:12:23', '2019-12-17 02:12:23'),
(2, '300hr YTTC', 'sdkfksd', '<p>skdksd</p>', '2019-12-19 09:42:36', '2019-12-19 09:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `all_fee_lists`
--

CREATE TABLE `all_fee_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `share_room` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `private_room` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all_fee_category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `all_fee_lists`
--

INSERT INTO `all_fee_lists` (`id`, `from`, `to`, `place`, `share_room`, `private_room`, `all_fee_category_id`, `created_at`, `updated_at`) VALUES
(1, '4th Feb (2020)', '4th Feb (2020)', 'Ilam', 'Now Offer Price: 1050 USD', 'Now Offer Price: 1250 USD', 1, '2019-12-17 02:38:08', '2019-12-21 06:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `apply_classes`
--

CREATE TABLE `apply_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all_fee_list_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apply_classes`
--

INSERT INTO `apply_classes` (`id`, `name`, `number`, `email`, `course`, `location`, `gender`, `room_type`, `status`, `all_fee_list_id`, `created_at`, `updated_at`) VALUES
(1, 'Riwash Chamlagain', '9825909867', 'A@a.com', '200 Hour Yoga Teacher Training In Rishikesh Fee & Schedule 2020', 'Ilam Nepal', 'male', 'share_room', 'unpaid', 1, '2019-12-21 05:49:15', '2019-12-21 05:49:15'),
(2, 'Riwash Chamlagain', '384569430964', 'technia@gmail.com', '200 Hour Yoga Teacher Training In Rishikesh Fee & Schedule 2020', 'Jhapa nepal', 'male', 'private_room', 'paid', 1, '2019-12-21 05:51:10', '2019-12-21 06:47:23'),
(3, 'Riwash Chamlagain', '9825909867', 'a@a.com', '200 Hour Yoga Teacher Training In Rishikesh Fee & Schedule 2020', 'jdcjsdjsjdjs', 'male', 'share_room', 'unpaid', 1, '2019-12-23 05:12:42', '2019-12-23 05:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `book_rooms`
--

CREATE TABLE `book_rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `check_in` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_out` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `children` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `number`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Riwash Chamlagain', 'tech@gmail.com', '9825909867', 'dsjfksd', 'thuis is', '2019-11-02 08:54:37', '2019-11-02 08:54:37'),
(2, 'Riwash Chamlagain', 'tech@gmail.com', '9825909867', 'dfsd', 'sdfsd', '2019-11-02 09:30:52', '2019-11-02 09:30:52'),
(3, 'Riwash Chamlagain', 'a@a.com', '9825909867', 'ndskskd', 'here is my ,message', '2019-12-20 01:48:00', '2019-12-20 01:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Ttile One', 'On the route of Annapurna trekking after few days Hiking from Pokhara, you\r\n\r\n\r\n\r\nwill be ready to discover theplace called\" Ghorepani Poon Hill \". A famous place for a view of Mount Annapurna Range, Dhaulagiri Range and Nilgiri. This place is also famous  flowering rhododendron forest, so for The Ghorepani Poonhill is a colorful short foray into the Annapurna region.  The trail winds through patchwork valleys, dense mossy forests, and past icy waterfalls. \r\nHotel Snow Land lies on a place called \"Deurali\" on the way to Ghorepani  Poon Hill, To discover the lifestyle of the people & places, taste the traditional dishes, meet the regulars of a bistro, participate in their traditions! You have a few days to enjoy this place of rich natural beauties of Nepal. As the Morning blaze with the beautiful skies and color of the rhododendron flowers , the exotic view of South Annapurna Range is seen from the hotel rooms', '191218083434gallery7.jpg', '2019-10-01 00:18:23', '2019-12-18 02:49:34'),
(2, 'title one 1', 'epani Poon Hill, To discover the lifestyle of the people & places, taste the traditional dishes, meet the regulars of a bistro, participate in their traditions! You have a few days to enjoy this place of rich natural beauties of Nepal. As the Morning blaze with the beautiful skies and color of the rhododendron flow', '191218083408gallery6.jpg', '2019-11-02 08:27:42', '2019-12-18 02:49:08');

-- --------------------------------------------------------

--
-- Table structure for table `fact_of_our_yogas`
--

CREATE TABLE `fact_of_our_yogas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fact_of_our_yogas`
--

INSERT INTO `fact_of_our_yogas` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'title one', '191218080222program5.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px;\">strongly recommend Rishikul Yogshala to everyone who want to practice yoga and who want to share his practice with others. I finished 200YTT in Rishikesh. I appreciated the effortless effort :) of the teachers to make us understand each subject of yogic science: asanas, pranayama, philosophy, anatomy, meditation, yoga nidra, cleansing practices. After one month you will</span><br></p>', '2019-12-18 02:17:22', '2019-12-18 02:17:22'),
(2, 'title one', '191218080229program5.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px;\">strongly recommend Rishikul Yogshala to everyone who want to practice yoga and who want to share his practice with others. I finished 200YTT in Rishikesh. I appreciated the effortless effort :) of the teachers to make us understand each subject of yogic science: asanas, pranayama, philosophy, anatomy, meditation, yoga nidra, cleansing practices. After one month you will</span><br></p>', '2019-12-18 02:17:29', '2019-12-18 02:17:29');

-- --------------------------------------------------------

--
-- Table structure for table `f_a_q_s`
--

CREATE TABLE `f_a_q_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `f_a_q_s`
--

INSERT INTO `f_a_q_s` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'what this site  for', '<p><span style=\"color: rgb(51, 51, 51); font-family: Poppins; font-size: 16px;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut sed adipiscing enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut sed adipiscing enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa.</span><br></p>', '2019-12-17 09:16:02', '2019-12-17 09:16:02'),
(2, 'Second Ask Question', '<p><span style=\"color: rgb(51, 51, 51); font-size: 16px; background-color: rgb(245, 245, 245);\">orem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut sed adipiscing enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut...</span><br></p>', '2019-12-20 01:01:22', '2019-12-20 01:01:22');

-- --------------------------------------------------------

--
-- Table structure for table `hour_yoga_teachers`
--

CREATE TABLE `hour_yoga_teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hour_yoga_teachers`
--

INSERT INTO `hour_yoga_teachers` (`id`, `title`, `date`, `content`, `created_at`, `updated_at`) VALUES
(1, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th August to 4th September (2020)', '<p><span id=\"docs-internal-guid-9bcf48b4-7fff-d51b-d67f-c037ebcf6b36\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, share Room: $ 1300</span></span><br></p>', '2019-12-13 07:12:18', '2019-12-25 01:42:19'),
(2, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th July to 4th August (2020)', '<p><span id=\"docs-internal-guid-a14bc8ac-7fff-3f78-ee11-ea7addf1d442\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, Share Room: $ 1300</span></span><br></p>', '2019-12-18 02:43:19', '2019-12-25 01:41:40'),
(3, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th June to 4th July (2020)', '<p><span id=\"docs-internal-guid-5c3313e9-7fff-ad2c-6571-b23bb130632c\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, Share Room: $ 1300</span></span><br></p>', '2019-12-18 02:43:57', '2019-12-25 01:41:04'),
(4, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th Sepetmber to 4th October (2020)', '<p><span id=\"docs-internal-guid-c0382e73-7fff-b132-1e49-c66e401e90aa\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, share Room: $ 1300</span></span><br></p>', '2019-12-25 01:43:14', '2019-12-25 01:43:14'),
(5, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th November to 4th December (2020)', '<p><span id=\"docs-internal-guid-e4c9ce19-7fff-1ffa-fa03-3671eceb768e\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, Share Room: $ 1300</span></span><br></p>', '2019-12-25 01:43:43', '2019-12-25 01:43:43'),
(6, '200 Hour Yoga Teacher Training in Pokhara Nepal', '7th December to 4th January (2020,2021)', '<p><span id=\"docs-internal-guid-d383688f-7fff-9e3c-39b2-b8c435e2eae5\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Single Room: $ 1500, Share Room: $ 1300</span></span><br></p>', '2019-12-25 01:44:15', '2019-12-25 01:44:15');

-- --------------------------------------------------------

--
-- Table structure for table `last_pages`
--

CREATE TABLE `last_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_page_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `last_pages`
--

INSERT INTO `last_pages` (`id`, `title`, `image`, `content`, `sub_page_id`, `created_at`, `updated_at`) VALUES
(1, 'sdlfvlsdlvvsd', '', '<p>kfdfvksdkvskdksd</p>', 1, '2019-12-17 01:43:09', '2019-12-17 01:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `logo`, `title`, `created_at`, `updated_at`) VALUES
(4, '19122209394680634961_772108186625103_2597261625488572416_n.png', 'Pokhara Yoga School', '2019-12-22 03:54:46', '2019-12-22 03:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `main_pages`
--

CREATE TABLE `main_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_pages`
--

INSERT INTO `main_pages` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Accomodation', '', '<p>this is content</p>', '2019-12-17 00:55:41', '2019-12-19 00:37:13'),
(2, 'Scholarship', '', '<p>here are some text</p>', '2019-12-17 01:04:38', '2019-12-19 00:34:06'),
(3, 'Yoga Course', '', '<p>here are alsooo</p>', '2019-12-17 01:05:01', '2019-12-19 00:33:33'),
(4, 'Location', '', '<p>dsnjsd</p>', '2019-12-19 00:39:18', '2019-12-19 00:39:18'),
(5, 'Curriculum', '', '<p>sdvmsdm</p>', '2019-12-19 00:39:57', '2019-12-19 00:39:57'),
(6, 'How to do Asanas', '', '<p>dsjsdj</p>', '2019-12-19 00:40:54', '2019-12-19 00:40:54'),
(7, 'Payment', '', '<p>dskfsjdkcsd</p>', '2019-12-19 00:41:10', '2019-12-19 00:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_20_151040_create_about_us_table', 1),
(4, '2019_09_20_151217_create_testimonials_table', 1),
(5, '2019_09_20_151321_create_photo_categories_table', 1),
(6, '2019_09_20_151413_create_photo_lists_table', 1),
(7, '2019_09_21_062454_create_sliders_table', 1),
(8, '2019_09_21_152352_create_contact_us_table', 1),
(9, '2019_09_22_054404_create_book_rooms_table', 1),
(11, '2019_10_01_053236_create_notices_table', 2),
(12, '2019_10_01_055550_create_courses_table', 3),
(13, '2019_10_01_060850_create_our_teams_table', 4),
(14, '2019_11_02_132545_create_logos_table', 5),
(16, '2019_12_13_121402_create_why_pokhara_yoga_fors_table', 6),
(18, '2019_12_13_123914_create_hour_yoga_teachers_table', 7),
(25, '2019_12_14_095200_create_yoga_teacher_training_nepals_table', 8),
(26, '2019_12_14_103042_create_fact_of_our_yogas_table', 8),
(27, '2019_12_15_100716_create_upcomming_event_places_table', 8),
(28, '2019_12_15_103153_create_upcomming_event_lists_table', 8),
(29, '2019_12_15_150521_create_special_offers_table', 9),
(30, '2019_12_15_153609_create_special_yoga_trainings_table', 10),
(32, '2019_12_17_061726_create_main_pages_table', 11),
(33, '2019_12_17_065024_create_sub_pages_table', 12),
(34, '2019_12_17_071406_create_last_pages_table', 13),
(35, '2019_12_17_073547_create_all_fee_categories_table', 14),
(36, '2019_12_17_074838_create_all_fee_categories_table', 15),
(37, '2019_12_17_080150_create_all_fee_lists_table', 16),
(38, '2019_12_17_144225_create_f_a_q_s_table', 17),
(39, '2019_12_18_071316_create_sliders_table', 18),
(41, '2019_12_18_074927_create_our_teams_table', 19),
(42, '2019_12_21_111500_create_apply_classes_table', 20),
(43, '2019_12_22_094305_create_other_information_table', 21);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_for` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `title`, `content`, `date_for`, `image`, `created_at`, `updated_at`) VALUES
(1, 'SLC Result comming soon', 'On the route of Annapurna trekking after few days Hiking from Pokhara, you\r\n\r\n\r\n\r\nwill be ready to discover theplace called\" Ghorepani Poon Hill \". A famous place for a view of Mount Annapurna Range, Dhaulagiri Range and Nilgiri. This place is also famous  flowering rhododendron forest, so for The Ghorepani Poonhill is a colorful short foray into the Annapurna region.  The trail winds through patchwork valleys, dense mossy forests, and past icy waterfalls. \r\nHotel Snow Land lies on a place called \"Deurali\" on the way to Ghorepani  Poon Hill, To discover the lifestyle of the people & places, taste the traditional dishes, meet the regulars of a bistro, participate in their traditions! You have a few days to enjoy this place of rich natural beauties of Nepal. As the Morning blaze with the beautiful skies and color of the rhododendron flowers , the exotic view of South Annapurna Range is seen from the hotel rooms', '2019-10-02', '191001055257download.png', '2019-10-01 00:03:10', '2019-10-01 00:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `other_information`
--

CREATE TABLE `other_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `other_information`
--

INSERT INTO `other_information` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Food', '191222095258program5.jpg', '<p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">The town of Pokhara is gifted with plush agricultural lands and beautiful scenery. Owing to this the town has an abundant supply of healthy diet choice extracted from fresh resources. Agriculture happens to be the major occupation in Nepal and this has truly been a boon for the country\'s fertile land. Even when eaten in a raw or semi-raw the food here tastes just as marvelous due to its pure nature. The food is served with a dash of scenic view from the restaurant.</span></p><p><span id=\"docs-internal-guid-b83455dd-7fff-65ab-e5f1-49ed9076e7bc\"></span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Vegetarian choices from garden prepared in authentic Nepali, Indian style for wholesome nourishment our target every day is to offer food that is beneficial for a healthy development of the body and mind. we strive to provide you with the best and the healthiest meat choices. If you have any particular aversions may notify us of the same in advance, so we can prepare just as delectable vegan meal choices for you.</span></p>', '2019-12-22 04:07:58', '2019-12-25 01:32:12'),
(2, 'FACILITIES AT THE CENTER', '', '<p><span id=\"docs-internal-guid-71a6ebb7-7fff-01e4-1f91-1a647dd90a04\"><span style=\"font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Hot and cold water with shower, swimming pool, specious garden, pick and drop facilities from nearby airport, travel and touring facilities available, free wifi servies food and accomodation.</span></span><br></p>', '2019-12-25 01:36:23', '2019-12-25 01:36:23'),
(3, 'STUDENT LIFE IN YOGA TEACHER TRAINING IN NEPAL', '', '<p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">The school believes in an all-round approach and has put forth delightful activities on the days off for students. Offering an insight into the town of Pokhara we take them to various destinations for hands on experience of the oriental landscapes. Here are some of the destinations covered during the Yoga Teacher Training in Pokhara Nepal.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Yoga in the mountains at sunrise</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Visit word peace pagoda, Devi, s Falls, Gupteshwor Mahadev Cave, Seti River Gorge.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">HOW TO REACH US IN NEPAL</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">By Airway: conmected internationally, the closest airport would be Tirbhuvan airport in Kathmandu. Multiple flights from various destinations run to and from Kathmandu. One can land at international airport in Kathmandu and take a bus, car or flight to Pokhara.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">By Land: if you are coming to Pokhara from India, you will find multiple points of entry into Nepal. Some of the entry points are Kakkarbhitta (west bengal), Biratnagar (Bihar) Birgunj (Bihar) Sanauli, Nepalgunj, Dhangadi and Mahendranagar all across the boarder from Uttar Pradesh. take a bus or Car ride to Pokhara.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">TRAINING OUTCOMES OF YOGA Pokhara Nepal (Pokhara Yoga School and Retreat Center)</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">With a Yoga TTC in Pokhara, Nepal you are rewarded with the most wonderful gifts and memories, few of these being:</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Certification that enables you as a yoga instructor on a global level.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Practice and insight into the most ancient science of yhe body, mind and soul.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Discussions and linkages with fellow yogis from across the globe.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">A chance to interact and well among yoga leaders and gurus.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Getting acquainted with the inner layers and understanding the science involved in forming a binding link mentally, physically and spiritually.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Take back exceptional knowledge and expertise that ensures a healthy lifestyle and diet for the entire life.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">YOGA TEACHER TRAINING POKHARA NEPAL 2020</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">200 Hour Yoga Teacher Training in Pokhara Nepal by Yoga Alliance certified school. Yoga teacher training certification courses by Pokhara Yoga School and Retreat Cenetr, A yoga alliance certified school.</span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">200 Hour Yoga Teacher Training in Pokhara Nepal</span></p><p><span id=\"docs-internal-guid-335affd5-7fff-8d50-904c-b8c5e4155806\"></span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">7th May to 4th June (2020)</span></p>', '2019-12-25 01:37:41', '2019-12-25 01:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `our_teams`
--

CREATE TABLE `our_teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `our_teams`
--

INSERT INTO `our_teams` (`id`, `name`, `desg`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Swami Atmananda', 'Yoga Teacher', '191218075302teacher1.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px; text-align: center;\">For several years, our exceptionally skilled instructors have been guiding and influencing the students to become the ruler of their own lives and to awaken their own inner sacredness. The yogic philosophy, empowering experiences, and immense knowledge of our teachers will encourage you to create an impactful change in your community.</span><br></p>', '2019-12-18 02:08:02', '2019-12-18 02:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photo_categories`
--

CREATE TABLE `photo_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo_categories`
--

INSERT INTO `photo_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'All', '2019-11-02 09:39:52', '2019-11-02 09:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `photo_lists`
--

CREATE TABLE `photo_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo_lists`
--

INSERT INTO `photo_lists` (`id`, `title`, `image`, `photo_category_id`, `created_at`, `updated_at`) VALUES
(1, 'sjdf', 'IMG-0356.jpg', 1, '2019-11-02 09:40:30', '2019-11-02 09:40:30'),
(2, 'sdfsdfc', 'IMG-0358.jpg', 1, '2019-11-02 09:40:44', '2019-11-02 09:40:44'),
(3, 'sd', 'IMG_0925.jpeg', 1, NULL, NULL),
(4, '', 'IMG-0281.jpg', 1, NULL, NULL),
(5, 'sd', 'IMG-0362.jpg', 1, NULL, NULL),
(6, '', 'IMG20191103125404.jpg', 1, NULL, NULL),
(7, '', 'IMG20191115083617.jpg', 1, NULL, NULL),
(8, '', 'IMG20191203104048.jpg', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Hour  Ayurveda and YTTC', 'IMG20191103125404.jpg', '<p>Hour  Ayurveda and YTTC traditions with 200 hour, 300 hour and 500 hour Ayurveda and Yoga TTC in Kerala, India. Get rewarded with Ayurveda and Yoga TTC at Rishikul Yogshala and become a certified Yoga Teacher and practitioner.<br></p>', '2019-12-18 01:31:38', '2019-12-18 01:31:54'),
(2, 'Hour  Ayurveda and YTTC traditions with 200 hour, 300 hour and 500', 'IMG-0362.jpg', '<p>Hour&nbsp; Ayurveda and YTTC traditions with 200 hour, 300 hour and 500Hour&nbsp; Ayurveda and YTTC traditions with 200 hour, 300 hour and 500<br></p>', '2019-12-18 01:32:28', '2019-12-18 01:32:28'),
(3, 'Hour Ayurveda and YTTC traditions with 200 hour, 300 hour and 500', 'IMG_0925.jpeg', '<p><span style=\"color: rgb(51, 51, 51); font-size: 16px; background-color: rgb(249, 249, 249);\">Hour Ayurveda and YTTC traditions with 200 hour, 300 hour and 500</span><br></p>', '2019-12-25 01:46:13', '2019-12-25 01:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `special_offers`
--

CREATE TABLE `special_offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `special_offers`
--

INSERT INTO `special_offers` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, '191218074019offer1.jpg', '2019-12-18 01:55:19', '2019-12-18 01:55:19'),
(3, '191218074033offer2.jpg', '2019-12-18 01:55:33', '2019-12-18 01:55:33'),
(4, '191218074056offer3.jpg', '2019-12-18 01:55:56', '2019-12-18 01:55:56');

-- --------------------------------------------------------

--
-- Table structure for table `special_yoga_trainings`
--

CREATE TABLE `special_yoga_trainings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `special_yoga_trainings`
--

INSERT INTO `special_yoga_trainings` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Adjustment and Alignment', '191218081951course1.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px; text-align: center;\">Step into the ‘World capital of yoga’ situated alongside the banks of the holy Ganges where it is believed that yoga &amp; meditation brings you one step closer to Moksha.</span><br></p>', '2019-12-15 10:04:12', '2019-12-18 02:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `sub_pages`
--

CREATE TABLE `sub_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_page_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_pages`
--

INSERT INTO `sub_pages` (`id`, `title`, `image`, `content`, `main_page_id`, `created_at`, `updated_at`) VALUES
(1, 'sub page a', '', '<p>fdjvkdfkvdkf</p>', 1, '2019-12-17 01:24:21', '2019-12-17 01:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Riwash Chamlagain', '191218080745testi2.png', '<span style=\"font-family: Poppins; font-size: 16px;\">I recently completed my 200hr yoga teacher training course at Rishikul Yogshala and I absolutely loved the experience. The environment facilitated a space for personal, physical and mental growth. Each teacher had a way of challenging and inspiring me both in their given field e.g. Ashtanga and as individual characters. The staff are lovely and will try their best to accommodate to all needs. Choosing to complete my teacher training in India has been most fruitful because I have had the opportunity to learn from an entirely different culture to my own and learn yoga in the traditional way. It\'s also well priced which is another indicator of how much they want to share and make yoga accessible to people.</span>', '2019-11-02 09:00:45', '2019-12-18 02:22:45'),
(2, 'ram bahadur', '191218080724testi1.png', '<span style=\"font-family: Poppins; font-size: 16px;\">I recently completed my 200hr yoga teacher training course at Rishikul Yogshala and I absolutely loved the experience. The environment facilitated a space for personal, physical and mental growth. Each teacher had a way of challenging and inspiring me both in their given field e.g. Ashtanga and as individual characters. The staff are lovely and will try their best to accommodate to all needs. Choosing to complete my teacher training in India has been most fruitful because I have had the opportunity to learn from an entirely different culture to my own and learn yoga in the traditional way. It\'s also well priced which is another indicator of how much they want to share and make yoga accessible to people.</span>', '2019-11-02 09:03:02', '2019-12-18 02:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `upcomming_event_lists`
--

CREATE TABLE `upcomming_event_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upcomming_event_place_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upcomming_event_lists`
--

INSERT INTO `upcomming_event_lists` (`id`, `title`, `date`, `price`, `upcomming_event_place_id`, `created_at`, `updated_at`) VALUES
(1, '200 Hour Yoga Teacher Training In Rishikesh', '12 july 2020', '$ 300', 1, '2019-12-15 05:07:14', '2019-12-15 05:07:14'),
(2, '300 Hour Yoga Teacher Training In Rishikesh', '7th March 2020 - 4th April (2020)', '$700 USD', 2, '2019-12-18 03:04:28', '2019-12-18 03:04:28');

-- --------------------------------------------------------

--
-- Table structure for table `upcomming_event_places`
--

CREATE TABLE `upcomming_event_places` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upcomming_event_places`
--

INSERT INTO `upcomming_event_places` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ilam', '2019-12-15 04:53:17', '2019-12-15 04:53:17'),
(2, 'Jhapa', '2019-12-18 02:58:54', '2019-12-18 02:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Riwash Chamlagain', 'admin@admin.com', NULL, '$2y$10$8AK1kX5F4q9s9Q5iJu59KutojmOtCRCXb5wA2bKdyD9aZX1l8pvcy', NULL, '2019-09-30 23:54:40', '2019-09-30 23:54:40');

-- --------------------------------------------------------

--
-- Table structure for table `why_pokhara_yoga_fors`
--

CREATE TABLE `why_pokhara_yoga_fors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `why_pokhara_yoga_fors`
--

INSERT INTO `why_pokhara_yoga_fors` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'WHY GO FOR YOGA TEACHER TRAINING IN NEPAL', '<p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">The ideal place for deep introsprection, self reflection and yoga is a combination of eye-soothing scenery, majestic serenity, traditionalism and solace. The valley of Pokhara expresses the depth of every such quality in its very essence. This is not all being in famous as one of the most ancient lands every such quality in its very essence. this is not all, being infamous as one of the most ancient lands where yogis, sages, spiritualists traveled and stepped foot on Pokhara Yoga School, s Yoga TTC in Pokhara Nepal comes as the perfect program in every sense of the word to explore on this crystalline land. we have always been very particular about the chosen destination and the places for imparting the holy education of yoga. The Idea behind welcoming YTTC in Pokhara, Nepal is an outcome of this beautiful approach and process.</span></p><p style=\"margin-bottom: 1rem; font-size: 16px; line-height: 24px; font-family: Poppins;\"><span id=\"docs-internal-guid-f6a8a65b-7fff-b2e8-a12c-2328c1cae983\"></span></p><p dir=\"ltr\" style=\"line-height:1.38;margin-top:0pt;margin-bottom:10pt;\"><span style=\"font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;\">Embark upon a unique expedition of yoga and spirituality in the lap of Himalayas with one of our Yoga Teacher Training Course in Pokhara Nepal, under the esteemed guidance of acclaimed yoga teacgers of Pokhara Yoga School. The mesmerizing river streams, ancient temples, giant pagodas, ice- laden beauty and plush agricultural fields, the soul of Pokhara Valley echoes with that of a yogi, s and spells the true  </span><span style=\"background-color: transparent; font-family: Calibri, sans-serif; font-size: 11pt; white-space: pre-wrap;\">guiding light towards attaining complete enlightenment and contentment of the mind, body and soul.</span></p>', '2019-12-13 06:39:44', '2019-12-25 01:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `yoga_teacher_training_nepals`
--

CREATE TABLE `yoga_teacher_training_nepals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `yoga_teacher_training_nepals`
--

INSERT INTO `yoga_teacher_training_nepals` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Rishikesh', '191218072722yoga1.jpeg', '<p><span style=\"font-family: Poppins; font-size: 16px; text-align: center;\">Step into the ‘World capital of yoga’ situated alongside the banks of the holy Ganges where it is believed that yoga &amp; meditation brings you one step closer to Moksha.</span><br></p>', '2019-12-18 01:42:22', '2019-12-18 01:42:22'),
(2, 'Kerala', '191218072757yoda2.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px; text-align: center;\">Step into the ‘World capital of yoga’ situated alongside the banks of the holy Ganges where it is believed that yoga &amp; meditation brings you one step closer to Moksha</span><br></p>', '2019-12-18 01:42:57', '2019-12-18 01:42:57'),
(3, 'Nepal', '191218072828yoga2.jpg', '<p><span style=\"font-family: Poppins; font-size: 16px; text-align: center;\">Step into the ‘World capital of yoga’ situated alongside the banks of the holy Ganges where it is believed that yoga &amp; meditation brings you one step closer to Moksha.</span><br></p>', '2019-12-18 01:43:28', '2019-12-18 01:43:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_fee_categories`
--
ALTER TABLE `all_fee_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_fee_lists`
--
ALTER TABLE `all_fee_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `all_fee_lists_all_fee_category_id_index` (`all_fee_category_id`);

--
-- Indexes for table `apply_classes`
--
ALTER TABLE `apply_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `apply_classes_all_fee_list_id_index` (`all_fee_list_id`);

--
-- Indexes for table `book_rooms`
--
ALTER TABLE `book_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fact_of_our_yogas`
--
ALTER TABLE `fact_of_our_yogas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `f_a_q_s`
--
ALTER TABLE `f_a_q_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hour_yoga_teachers`
--
ALTER TABLE `hour_yoga_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_pages`
--
ALTER TABLE `last_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_pages_sub_page_id_index` (`sub_page_id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_pages`
--
ALTER TABLE `main_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_information`
--
ALTER TABLE `other_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_teams`
--
ALTER TABLE `our_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photo_categories`
--
ALTER TABLE `photo_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_lists`
--
ALTER TABLE `photo_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photo_lists_photo_category_id_index` (`photo_category_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_offers`
--
ALTER TABLE `special_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_yoga_trainings`
--
ALTER TABLE `special_yoga_trainings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_pages`
--
ALTER TABLE `sub_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_pages_main_page_id_index` (`main_page_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upcomming_event_lists`
--
ALTER TABLE `upcomming_event_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upcomming_event_lists_upcomming_event_place_id_index` (`upcomming_event_place_id`);

--
-- Indexes for table `upcomming_event_places`
--
ALTER TABLE `upcomming_event_places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `why_pokhara_yoga_fors`
--
ALTER TABLE `why_pokhara_yoga_fors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yoga_teacher_training_nepals`
--
ALTER TABLE `yoga_teacher_training_nepals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `all_fee_categories`
--
ALTER TABLE `all_fee_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `all_fee_lists`
--
ALTER TABLE `all_fee_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apply_classes`
--
ALTER TABLE `apply_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_rooms`
--
ALTER TABLE `book_rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fact_of_our_yogas`
--
ALTER TABLE `fact_of_our_yogas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `f_a_q_s`
--
ALTER TABLE `f_a_q_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hour_yoga_teachers`
--
ALTER TABLE `hour_yoga_teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `last_pages`
--
ALTER TABLE `last_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main_pages`
--
ALTER TABLE `main_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `other_information`
--
ALTER TABLE `other_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `our_teams`
--
ALTER TABLE `our_teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photo_categories`
--
ALTER TABLE `photo_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photo_lists`
--
ALTER TABLE `photo_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `special_offers`
--
ALTER TABLE `special_offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `special_yoga_trainings`
--
ALTER TABLE `special_yoga_trainings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_pages`
--
ALTER TABLE `sub_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upcomming_event_lists`
--
ALTER TABLE `upcomming_event_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upcomming_event_places`
--
ALTER TABLE `upcomming_event_places`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `why_pokhara_yoga_fors`
--
ALTER TABLE `why_pokhara_yoga_fors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `yoga_teacher_training_nepals`
--
ALTER TABLE `yoga_teacher_training_nepals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `all_fee_lists`
--
ALTER TABLE `all_fee_lists`
  ADD CONSTRAINT `all_fee_lists_all_fee_category_id_foreign` FOREIGN KEY (`all_fee_category_id`) REFERENCES `all_fee_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `apply_classes`
--
ALTER TABLE `apply_classes`
  ADD CONSTRAINT `apply_classes_all_fee_list_id_foreign` FOREIGN KEY (`all_fee_list_id`) REFERENCES `all_fee_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `last_pages`
--
ALTER TABLE `last_pages`
  ADD CONSTRAINT `last_pages_sub_page_id_foreign` FOREIGN KEY (`sub_page_id`) REFERENCES `sub_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photo_lists`
--
ALTER TABLE `photo_lists`
  ADD CONSTRAINT `photo_lists_photo_category_id_foreign` FOREIGN KEY (`photo_category_id`) REFERENCES `photo_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_pages`
--
ALTER TABLE `sub_pages`
  ADD CONSTRAINT `sub_pages_main_page_id_foreign` FOREIGN KEY (`main_page_id`) REFERENCES `main_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `upcomming_event_lists`
--
ALTER TABLE `upcomming_event_lists`
  ADD CONSTRAINT `upcomming_event_lists_upcomming_event_place_id_foreign` FOREIGN KEY (`upcomming_event_place_id`) REFERENCES `upcomming_event_places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
