
@section("title", "Home Page")

 @section("home")
  @endsection
 @include('front.includes.header')
 

 
<div class="slider">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <li data-target="#carouselExampleCaptions" data-slide-to="{{$c}}" class="@if($c == 1) active @else @endif"></li>
      @php $c++; @endphp
      
     @endforeach
    </ol>
    <div class="carousel-inner">
      @php $c = 1; @endphp
      @foreach($sliders as $slider)
      <div class="carousel-item @if($c == 1) active @endif">
        <img src="{{asset('uploads/'.$slider->image)}}" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>{{$slider->title}} </h5>
          <p>{!! $slider->content !!}</p>
         {{--  <a href="#" class="btn btn-banner1">200hr</a>
          <a href="#" class="btn btn-banner2">300hr</a>
          <a href="#" class="btn btn-banner3">400hr</a>
          <a href="#" class="btn btn-book">Book Now</a> --}}
        </div>
      </div>
      @php $c++; @endphp
      
     @endforeach
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a> -->
  </div>
</div>
@if($yogaTraningNepal->isEmpty())
@else
<div class="teaching-section">
  <div class="container">
    <dive class="row">
      <div class="col-12 col-sm-12">
        <h3>Yoga Teacher Training In Nepal</h3>
      </div>
      @foreach($yogaTraningNepal as $item)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$item->image)}}" alt="">
        <div class="teach-box">
          <h4>{{$item->title}}</h4>
          <p>{{ str_limit(strip_tags($item->content), 250) }}</p>
          <a href="{{action('Front\FrontController@yogaTrainingNepal',$item->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
  
</div>
@endif

<div class="about-section">
  <div class="container">
    <div class="row">
      @foreach($aboutUs as $about)
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="about-box">
          <h4>{{$about->title}}</h4>
          <p class="text-justify">
          {!! $about->content !!}
        </p>
        </div>
      </div>
      @endforeach
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="video-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="video-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/p9X16AB-3tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<section id="tabs">
  <div class="container">
    <h6 class="section-title h1">Yoga Teacher Training Upcoming Events</h6>
    <div class="row">
      <div class="col-12 col-sm-12">
        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                @php $c = 1 @endphp
               @foreach($events as $event)
            <a class="nav-item nav-link @if($c == 1) active @endif" id="nav-home-tab" data-toggle="tab" href="#nav-home{{$event->id}}" role="tab" aria-controls="nav-home{{$event->id}}" aria-selected="true">{{$event->name}}</a>
            @php $c++ @endphp
            @endforeach

           <!--  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Kerela</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Nepal</a> -->
          </div>
        </nav>
   
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          @php $c = 1 @endphp
        @foreach($events as $event)

          <div class="tab-pane fade show @if($c == 1) active @endif" id="nav-home{{$event->id}}" role="tabpanel" aria-labelledby="nav-home-tab{{$event->id}}">
            <div class="table-responsive" id="sailorTableArea">
              <table id="sailorTable" class="table table-striped table-bordered" width="100%">
                <tbody>
                  @foreach($event->upcommingEventList as $eventList)
                    <tr>
                        <td>{{$eventList->title}}</td>
                        <td>{{$eventList->date}}</td>
                        <td>{{$eventList->price}}</td>
                    </tr>
                    @endforeach
                    
                    
                </tbody>
              </table>
            </div>
          </div>
          @php $c++ @endphp

          @endforeach
        </div>


      
      </div>
    </div>
  </div>
</section>
@if(!$specialOffer->isEmpty())
<div class="special">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Special Offers for Yoga Teacher Training</h4>
      </div>
      @foreach($specialOffer as $list)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$list->image)}}" alt="">
        <a href="#" class="btn btn-offer">Book Now <i class="fa fa-angle-double-right"></i></a>
      </div>
      @endforeach
      
    </div>
  </div>
</div>
@else
@endif

@if($otherInformation->isEmpty())
@else
<div class="teaching-section">
  <div class="container">
    <dive class="row">
      <div class="col-12 col-sm-12">
        <h3>Yoga Teacher Training In Nepal</h3>
      </div>
      @foreach($otherInformation as $other)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$other->image)}}" alt="" style="height:300px">
        <div class="teach-box">
          <h4>{{$other->title}}</h4>
          <p>{{ str_limit(strip_tags($other->content), 200) }}</p>
          <a href="{{action('Front\FrontController@singleOther',$other->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
      
      @endforeach
    </div>
  </div>
  
</div>
@endif



@if(!$ourTeam->isEmpty())
<div class="yoga-teacher">
  <div class="container px-5">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Our Yoga Teachers</h4>
        <p>For several years, our exceptionally skilled instructors have been guiding and influencing the students to become the ruler of their own lives and to awaken their own inner sacredness. The yogic philosophy, empowering experiences, and immense knowledge of our teachers will encourage you to create an impactful change in your community.</p>
      </div>
      @foreach($ourTeam as $team)
      <div class="col-12 col-sm-12 col-md-6 col-lg-4">
        <img src="{{asset('uploads/'.$team->image)}}" alt="" style="height:300px">
        <h6>{{$team->name}}</h6>
        <a href="{{action('Front\FrontController@teacher',$team->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
      </div>
      @endforeach
      
      
      <div class="col-12 col-sm-12">
        <a href="#" class="btn btn-teacher">View More Teacher <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif


@if(!$uniqueFact->isEmpty())
<div class="program-section">
  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-12">
        <h4>The Unique Facets of our Yoga Teacher Training Programs</h4>
      </div>
       @foreach($uniqueFact as $fact)

      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$fact->image)}}" alt="">
        <div class="teach-box">
          <h4>{{$fact->title}}</h4>
          <p>{!! str_limit($fact->content, 300) !!}</p>
          <a href="{{action('Front\FrontController@uniquefact',$fact->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
 @endforeach
    
     
    </div>
  </div>
</div>
@else
@endif


@if(!$testimonials->isEmpty())
<div class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Yoga Teacher Training Student Testimonials</h4>
      </div>
      @foreach($testimonials as $test)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <div class="testi-box">
          <img src="{{asset('uploads/'.$test->image)}}" alt="">
          <h6><i class="fa fa-quote-left"></i> {{$test->name}}</h6>
          <p>{!! $test->content !!}</p>
          <p><strong>Thank You</strong></p>
        </div>
      </div>
      @endforeach

    
      <div class="col-12 col-sm-12">
        <a href="{{action('Front\FrontController@testimonial')}}" class="btn btn-testi">Read More Testimonials <i class="fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
</div>
@else
@endif

@if(!$yogaCourse->isEmpty())
<div class="teaching-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Special Yoga Teacher Training Courses</h3>
      </div>
      @foreach($yogaCourse as $course)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <img src="{{asset('uploads/'.$course->image)}}" alt="">
        <div class="teach-box">
          <h4>{{$course->title}}</h4>
          <p>{!! str_limit($course->content, 250) !!}</p><br>
          <a href="{{action('Front\FrontController@specialYoga',$course->id)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach
    </div>
  </div>
</div>
@else
@endif

<div class="teacher-training">
  <div class="container">
    <div class="row">
      @foreach($whyYoga as $why)
      <div class="col-12 col-sm-12">
        <div class="tech-box">
          <h4>{{$why->title}}</h4>
          <p>{!!  $why->content !!}</p>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
<div class="training-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4><a href="#">200 Hour Yoga Teacher Training in Pokhara Nepal 2020</a></h4>
        <p class="hour">200 hour yoga teacher training in Pokhara Nepal by yoga alliance certified school. </p>
      </div>
      @foreach($hourYoga as $hour)
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        
        <div class="class-box">
          <h6>{{$hour->title}} <span>{{$hour->date}}</span></h6>
          <p>{!! $hour->content !!}</p>
        </div>
      
        
      </div>
      @endforeach
    </div>
  </div>
</div>
@if(!$blogs->isEmpty())
<div class="teaching-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Our Blogs</h3>
      </div>
      @foreach($blogs as $blog)
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        @if(!$blog->image == '')
        <img src="{{asset('uploads/'.$blog->image)}}" alt="">
        @else
        @endif

        <div class="teach-box">
          <h4>{{$blog->title}}</h4>
          <ul>
            <li><i class="fa fa-user"></i> by <a href="#">Admin</a></li>
            <li><i class="fa fa-calendar"></i>{{ $blog->created_at->format('j M, Y') }}</li>
          </ul>
          <p>{!! str_limit($blog->content, 200) !!}</p>
          <a href="{{action('Front\FrontController@singleBlog',$blog->slug)}}" class="btn btn-read">Read More <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>


      @endforeach

    </div>
  </div>
</div>
@else
@endif

@include('front.includes.footer')