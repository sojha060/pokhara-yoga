 @section("title", "$information->title")
  @section("desc", "$information->des")
  @section("keyword", "$information->keyword")
 @section("singlepage")
  @endsection
@include('front.includes.header')

<div class="page-banner">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h1>{{$information->title}}</h1>
          <ul class="breadcrumb">
            <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
            <li>{{$information->title}}</li>
         

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="about-body text-justify">
  <div class="container">
    <div class="social-icons pb-2">
  @include('front.includes.social-media')
</div>
          <h2 class="text-center">{{$information->title}}</h2>

    <div class="row">
      @if(!$information->image == '')
     <div class="col-12 col-sm-12 col-md-4 col-lg-4 detail-left">
        <img src="{{asset('uploads/'.$information->image)}}" alt="" class="img-fluid">
        
      </div>
      @else
      @endif
      <div class="col-12 col-sm-12 @if($information->image == '') col-md-12 col-lg-12 @else col-md-6 col-lg-6 @endif ">
        @if($information->name)
        <h5> <strong>Name:</strong><span class="pr-1"> </span>{{$information->name}}</h5>
@else 
@endif
        <p>{!!  $information->content !!}</p>

        
      </div>
    </div>
  </div>
</div>



{{-- <div class="faqs-body">
  <div class="container">
    <div class="social-icons pb-2">
</div>
    <div class="row">
      <div class="col-12 col-sm-12">

      </div> --}}

@if(is_null($information->mainPageDrop))
   @else

      @foreach($information->mainPageDrop as $drop)
      <div class="col-12 col-sm-12 col-sm-6 col-md-6">
        <div class="accordion-container">
          <div class="set">
            <a href="javascript:void(0)">
              {{ $drop->title }}
              <i class="fa fa-plus"></i>
            </a>
            <div class="content" style="display: none;">
                <p>{!! $drop->content !!}</p>
            </div>
          </div>

         
         
        </div>
      </div>
      @endforeach
   
      @endif
     


      
    </div>
  </div>
</div>





@include('front.includes.footer')