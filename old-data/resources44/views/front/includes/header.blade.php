   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   @foreach($logos as $logo)
   @foreach($metaTags as $tag)
    <title>{{$logo->title}} | @yield('title')</title>
   
    <meta property="og:url"           content="{{url()->current()}}" />
  <meta property="og:type"          content="Pokhara Yoga School" />
  <meta property="og:title"         content=" @yield('title')" />
  <meta property="og:description"   content="{{$tag->des}} || @yield('desc')" />
  <meta name="author" content="{{ $tag->auth }}">
  <meta name="description" content="{{$tag->des}} || @yield('desc')">
  <meta name="keywords" content="{{$tag->keyword}} || @yield('keyword')">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   @endforeach
   @endforeach
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">

    <!--Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('front/css/all.css')}}">

    <!--Main CSS-->
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.css">

    <!--Owl Carousel-->
    <link rel="stylesheet" href="{{asset('front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/owl.theme.default.min.css')}}">
    

</head>
<body>
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>

<header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <div class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 header-left">
          <ul>
            <li><a href="#"><i class="fa fa-phone-volume"></i> +977-1-1234567</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> test@test.com</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 header-right">
          <ul>
            <li><a href="{{ action('Front\FrontController@contactUs') }}" class="applys">Contact Now <i class="fas fa-pen-square"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="menu-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-10">
        <nav class="navbar navbar-expand-lg navbar-light">
   @foreach($logos as $logo)

          <a class="navbar-brand" href="{{action('Front\FrontController@index')}}"><img src="{{asset('uploads/'.$logo->logo)}}" alt=""></a>
          @endforeach

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto">
              <li class="nav-item active">
                <a class="nav-link" href="{{action('Front\FrontController@index')}}">Home</a>
              </li>
             @foreach($mainPage as $page)
             
              <li class="nav-item dropdown ">
                <a class="nav-link" href="{{action('Front\FrontController@singleMainPage',$page->slug)}}">{{$page->title}}</a>
                <ul>
                  @foreach($page->subPage->sortBy('order') as $subPage)
                  <li><a href="{{action('Front\FrontController@singlePage',$subPage->slug)}}">{{$subPage->title}}</a></li>
                  @endforeach
                  
                </ul>
              </li>


              
              @endforeach
               <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@photoList')}}">Gallery</a>
              </li>
               <li class="nav-item ">
                <a class="nav-link" href="{{action('Front\FrontController@contactUs')}}">Contact Us</a>
              </li>
            </ul>
          </div>
        </nav>
        <div class="mobile-header">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-12">
              <span class="clickmenus" onclick="openNav()">&#9776; </span>
              <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="mobile-menus">
                  <ul>
                    <li><a href="{{action('Front\FrontController@index')}}">Home</a></li>
                    @foreach($mainPage as $page)
                    <li>
                      <a href="{{action('Front\FrontController@singleMainPage',$page->slug)}}" type="text" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">{{$page->title}} <i class="wsmenu-arrow fa @if(!$page->subPage->isEmpty()) fa-angle-down @else @endif"></i></a>
                      <div class="collapse multi-collapse" id="multiCollapseExample2">
                        <div class="card card-body">
                        <ul>
                          @foreach($page->subPage->sortBy('order') as $subPage)
                  <li><a href="{{action('Front\FrontController@singlePage',$subPage->slug)}}">{{$subPage->title}}</a></li>
                  @endforeach
                        
                        </ul>
                      </div>
                    </li>
                    @endforeach
                    
                    
                  </ul>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="col-12 col-sm-12 col-md-2 col-lg-2">
        <div class="menu-adds">
          {{-- <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p> --}}
          {{-- <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
      <div class="col-12 col-sm-12">
        <div class="menu-adds-mobile">
         {{--  <p><i class="fa fa-phone-volume"></i> +977-1-1234567</p> --}}
         {{--  <p><i class="fa fa-envelope"></i> info@pokharayogaschoolandretreatcenter.com</p> --}}
        </div>
      </div>
    </div>
  </div>
</header>